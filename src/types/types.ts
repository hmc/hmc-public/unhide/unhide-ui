import { StaticImport } from "next/dist/shared/lib/get-img-props";
import { z } from "zod";

import {
  SOLR_RESULT_ITEM_KEYS,
  SOLR_RESULT_CONFIG_KEYS,
  SOLR_CATEGORIES,
} from "@/app/api/config/config";

// ================== SOLR DATA SCHEMES =================
export type Formats = {
  text: string;
  type: string;
  nStartYear: string;
  nEndYear: string;
};

export type SolrResponseType = {
  responseHeader: Record<string, number | Record<string, string | string[]>>;
  response: {
    numFound: number;
    start: number;
    numFoundExact: boolean;
    docs: SolrResponseDocsType[];
  };
  facet_counts: {
    facet_queries: object;
    facet_fields: { type: SOLRCountsArray };
    facet_ranges: object;
    facet_intervals: object;
    facet_heatmaps: object;
  };
};

export type SolrResponseDocsType = {
  keys: ResultItemKeys[];
  json_source: string;
  json_uplifted: string;
  index_id: string;
  _version_: string;
  indexed_ts: string;
} & ResultDocs;

// this is a pattern combining three ts
// https://learntypescript.dev/10/l5-deep-immutable#using-const-assertions
// https://www.typescriptlang.org/docs/handbook/2/typeof-types.html
// https://www.typescriptlang.org/docs/handbook/2/indexed-access-types.html
type ResultItemKeyTuple = typeof SOLR_RESULT_ITEM_KEYS;
export type ResultItemKeys = ResultItemKeyTuple[number];

type ResultConfigKeyTuple = typeof SOLR_RESULT_CONFIG_KEYS;
export type ResultConfigKeys = ResultConfigKeyTuple[number];

type categoryTuple = typeof SOLR_CATEGORIES;
export type SOLRCategories = categoryTuple[number];

export type SOLRCountsArray = (SOLRCategories | number)[];

// =========== MODIFIED DATA SCHEMES ==============
export type ResultDocs = {
  id: string;
  name: string;
  type: string;
  txt_identifier: string | string[];
  description: string;
  txt_keywords: string | string[];
  txt_includedInDataCatalog: string;
  txt_sameAs?: string;
  txt_email?: string;
  txt_affiliation?: string;
  txt_author?: string;
  txt_creator?: string;
  txt_publisher?: string;
  txt_contributor?: string;
  txt_version?: string | string[];
  txt_license?: string | string[];
  txt_url?: string | string[];
  txt_codeRepository?: string | string[];
  txt_provider?: string;
  txt_endDate?: string;
  txt_location?: string;
  txt_startDate?: string;
  txt_performer?: string;
};

export type ResultConfig = {
  recordConfig: {
    index_id: string;
    _version_: string;
    indexed_ts: string;
  };
};

export type ResultKeys = { keys: ResultItemKeys[] };

type RorResultItemBase = {
  id: string;
  name: string;
  acronyms?: string[];
  aliases?: string[];
  country?: {
    country_name: string;
    country_code: string;
  };
  email_address?: string | null;
  established: number;
  external_ids?: {
    FundRef?: {
      preferred: null | string;
      all: string[];
    };
    GRID?: {
      preferred: null | string;
      all: string[];
    };
    ISNI?: {
      preferred: null | string;
      all: string[];
    };
    OrgRef?: {
      preferred: null | string;
      all: string[];
    };
    Wikidata?: {
      preferred: null | string;
      all: string[];
    };
  };
  links: string[];
  relationships?: {
    label: string;
    type: "Parent" | "Child" | "Related";
    id: string;
  }[];
  status: string;
  types?: string[];
  wikipedia_url?: string;
};

/*
definition of Required Utility in TS - https://www.typescriptlang.org/docs/handbook/utility-types.html#requiredtype
type Required<T> = {
  [P in keyof T]-?: T[P]
}
used in to create a generic type that enforces "required" on a particular property - https://www.emmanuelgautier.com/blog/snippets/typescript-required-properties
*/
type WithRequired<T, K extends keyof T> = T & { [P in K]-?: T[P] };

export type RorResultItem = WithRequired<RorResultItemBase, "established">;

export type ResultItem = ResultDocs & ResultConfig & ResultKeys & RorResultItem;

// useless for now, later if we type the entities this will help
export type FilterProps = string;
export type FilterEntity = {
  filterName: FilterProps;
  itemName: string;
  itemCount: number;
};

export type SearchResponse = {
  records: ResultItem[];
  totalCount: number;
  categoryCounts: Record<FacetFieldKeys, number>;
  filters: FilterEntity[];
};

//----- CATEGORIES API ------
export type FacetIntervalFieldKeys = "documents" | "datasets";
export type FacetFieldKeys =
  | FacetIntervalFieldKeys
  | "experts"
  | "institutions"
  | "trainings"
  | "projects"
  | "software"
  | "instruments";

export type Category = {
  id: string;
  text: string;
  icon?: StaticImport;
  count: number;
};

// -------- SEARCH API ---------
export const searchSchema = z.object({
  searchText: z.string().optional(),
  documentType:
    // TODO: make the validation error object smaller and more precise, possibly for all validations here!
    z
      .union([
        z.literal("experts"),
        z.literal("institutions"),
        z.literal("datasets"),
        z.literal("documents"),
        z.literal("trainings"),
        z.literal("projects"),
        z.literal("software"),
        z.literal("instruments"),
      ])
      .optional(),
  region: z.string().optional(),
  facetType: z.string().array().default([]),
  facetName: z.string().array().default([]),
  start: z.coerce.number().min(0).default(0),
  rows: z.coerce.number().min(0).max(10).default(10),
  includeFacets: z.coerce.boolean().default(true),
});

export type searchParamsType = z.infer<typeof searchSchema>;
