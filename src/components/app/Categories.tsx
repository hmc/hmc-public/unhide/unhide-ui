"use client";

import Image from "next/image";
import { useTranslations } from "next-intl";
import { useEffect, useState } from "react";

import Error from "@/components/layout/Error";
import Spinner from "@/components/layout/Spinner";
import { Link } from "@/i18n/routing";
import type { Category } from "@/types/types";
import { getAllCategories } from "@/utils/apiCall";

const Categories = () => {
  const t = useTranslations("Categories");
  const [allCategories, setCategories] = useState<Category[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [hasError, setHasError] = useState<boolean>(false);

  useEffect(() => {
    const updateCategories = async () => {
      try {
        setIsLoading(true);
        setHasError(false);
        const categories = await getAllCategories();
        for (const category of categories) {
          let iconImport;

          try {
            iconImport = await import("@/resources/images/category/" + category.id + ".png");
          } catch {
            iconImport = await import("@/resources/images/category/software.png");
          }

          category.icon = iconImport.default;
        }
        setCategories(categories);
        setIsLoading(false);
      } catch {
        setIsLoading(false);
        setHasError(true);
      }
    };
    updateCategories();
  }, []);

  const formatter = Intl.NumberFormat("en", { notation: "compact" });
  /* traditional JS module that makes the badge numbers appear as strings representing exponents
      check - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat/NumberFormat#options */

  return (
    <div className="grow flex flex-col">
      {isLoading ? (
        <Spinner />
      ) : hasError ? (
        <Error />
      ) : (
        <div className="grow container mx-auto flex flex-col justify-center pb-16 md:p-14 md:pt-2 lg:p-20 lg:pt-10 xl:pt-10 xl:pb-20 2xl:pb-16 3xl:py-0">
          <div className="p-8 md:px-0 xl:pb-10 text-xl font-normal text-primary-helmholtz-dunkelblau">
            {t("categories")}
          </div>
          {/* grid of categories */}
          <div className="px-10 grid grid-cols-1  md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 3xl:grid-cols-5 gap-y-28 md:gap-x-16 lg:gap-x-32 xl:gap-x-46 content-center justify-center">
            {allCategories.map((o) => {
              return (
                <Link
                  key={o.id}
                  href={`/results?category=${o.id}`}
                  className="group flex place-content-center transition ease-in-out hover:-translate-y-1 hover:scale-110"
                >
                  {/* icon for category */}
                  <Image
                    src={o.icon || ""}
                    alt={t(o.id, { count: o.count })}
                    className="z-10 -m-3 h-20 w-20 rounded-full border-2 border-secondary-helmholtz-mint bg-primary-helmholtz-dunkelblau p-4 group-hover:border-primary-helmholtz-hellblau"
                  />

                  {/* action button for category */}
                  <div
                    key={o.id}
                    className="-ml-6 flex space-x-1 rounded-full border-2 border-secondary-helmholtz-mint px-16 lg:px-20 shadow group-hover:border-primary-helmholtz-hellblau"
                  >
                    <div className="flex place-self-center text-primary-helmholtz-dunkelblau group-hover:text-primary-helmholtz-hellblau">
                      {t(o.id, { count: o.count })}
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="h-7 w-4 place-self-center pl-1 transition delay-150 ease-in-out group-hover:translate-x-2 group-hover:scale-110"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
                        />
                      </svg>
                    </div>
                  </div>

                  <span className="relative -m-6 h-12 w-12 rounded-full border-2 border-secondary-helmholtz-mint group-hover:border-primary-helmholtz-hellblau">
                    <button className="opacity-65 absolute inline-flex h-full w-full rounded-full bg-primary-helmholtz-hellblau" />
                    <span className="relative inline-flex h-11 w-11 place-content-center rounded-full bg-primary-helmholtz-hellblau object-contain align-middle">
                      <span className="self-center font-semibold text-primary-helmholtz-weiss">
                        {formatter.format(o.count)}
                      </span>
                    </span>
                  </span>
                </Link>
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
};

export default Categories;
