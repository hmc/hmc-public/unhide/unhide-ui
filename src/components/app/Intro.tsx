import Image from "next/image";
import { useTranslations } from "next-intl";
import { ReactNode } from "react";

import ExternalLink from "@/components/ExternalLink";
import logoHGF from "@/resources/images/logo/Logo_HGF-KG_text_brightback.png";
import bg from "@/resources/images/misc/unhide_kg_tbg.png";
import InfoIcon from "@/resources/images/svg/InfoIcon";

const Intro = () => {
  const t = useTranslations("Intro");

  return (
    <>
      <div
        className="hidden lg:flex cursor-default justify-start border-2 border-b-secondary-helmholtz-highlightblau bg-repeat-y p-20 bg-right-bottom bg-blend-lighten"
        style={{
          backgroundImage: `url(${bg.src})`,
        }}
      >
        <div className="sm:basis-9/12 md:basis-5/6 backdrop-blur-sm backdrop-opacity-60">
          {/* quote container */}
          <div className="text-primary-helmholtz-dunkelblau ">
            <h3 className="text-xl font-normal italic ">&quot;{t("quote_text")}&quot;</h3>
            <div className="before:content-['-_'] text-sm font-thin">{t("quote_author")}</div>
          </div>

          {/* intro container  */}
          <div className="mt-10 basis-1/2 justify-start text-primary-helmholtz-dunkelblau">
            {t.rich("intro_text", {
              highlight: (content: ReactNode) => <span className="font-bold">{content}</span>,
              linkUnHide: (content: ReactNode) => (
                <ExternalLink
                  underlined
                  href="https://codebase.helmholtz.cloud/hmc/hmc-public/unhide"
                >
                  {content}
                </ExternalLink>
              ),
              linkHelmholtz: (content: ReactNode) => (
                <ExternalLink underlined href="https://www.helmholtz.de/en/">
                  {content}
                </ExternalLink>
              ),
              linkHMC: (content: ReactNode) => (
                <ExternalLink underlined href="https://helmholtz-metadaten.de/en">
                  {content}
                </ExternalLink>
              ),
            })}
          </div>

          <div className="text-md text-primary-helmholtz-dunkelblau">
            <div className="flex mt-8">
              <InfoIcon />
              <p>
                {t.rich("learn_more", {
                  link: (content) => (
                    <ExternalLink
                      underlined
                      href={
                        process.env.NEXT_PUBLIC_DOC_URL ||
                        "https://docs.unhide.helmholtz-metadaten.de"
                      }
                    >
                      {content}
                    </ExternalLink>
                  ),
                })}
              </p>
            </div>
            <div className="flex mt-8">
              <InfoIcon />
              <p>
                {t.rich("find_sparql", {
                  link: (content) => (
                    <ExternalLink
                      underlined
                      href={
                        process.env.NEXT_PUBLIC_SPARQL_URL ||
                        "https://sparql.unhide.helmholtz-metadaten.de"
                      }
                    >
                      {content}
                    </ExternalLink>
                  ),
                })}
              </p>
            </div>
          </div>
        </div>
        <div className="min-h-full flex flex-col-reverse sm:basis-3/12 md:basis-1/6 items-end">
          <Image
            src={logoHGF}
            className="sm:max-w-42 sm:max-h-28 md:max-w-48 md:max-h-32"
            alt="Helmholtz KG Logo"
          />
        </div>
      </div>
    </>
  );
};

export default Intro;
