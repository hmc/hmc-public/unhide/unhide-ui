import { useTranslations } from "next-intl";

const NoResults = () => {
  const t = useTranslations("ListResults");
  return <div className="text-center my-10">{t("no_results")}</div>;
};

export default NoResults;
