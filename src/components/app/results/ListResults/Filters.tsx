import { Dialog } from "@headlessui/react";
import { useTranslations } from "next-intl";
import { useEffect, useState } from "react";

import MultiCheckFilter from "@/components/app/results/ListResults/Filters/MultiCheckFilter";
import type { FilterEntity, FilterProps } from "@/types/types";

type Props = {
  availableFilters: FilterEntity[];
  selectedFilters: FilterEntity[];
  setSelectedFilters: (newFilters: FilterEntity[]) => void;
};

const Filters = ({ availableFilters, selectedFilters, setSelectedFilters }: Props) => {
  const t = useTranslations("Filters");
  const s = useTranslations("BackendIds");
  type myObj = Record<FilterProps, FilterEntity[]>;
  const [filtersPerName, setFiltersPerName] = useState<myObj>({});
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const tmp: myObj = {};
    availableFilters.forEach((entity) => {
      if (entity.filterName in tmp) {
        tmp[entity.filterName].push(entity);
      } else {
        tmp[entity.filterName] = [entity];
      }
    });

    setFiltersPerName(tmp);
  }, [availableFilters]);

  const unselectFilterByFilerName = (filterName: FilterProps) => {
    const newSelected = structuredClone(selectedFilters);
    setSelectedFilters(newSelected.filter((entity) => entity.filterName !== filterName));
  };

  const toggleFilter = (entity: FilterEntity) => {
    const newSelected = structuredClone(selectedFilters);
    const index = newSelected.findIndex(
      (selected) =>
        selected.filterName === entity.filterName && selected.itemName === entity.itemName
    );

    if (index > -1) {
      newSelected.splice(index, 1);
    } else {
      newSelected.push(entity);
    }

    setSelectedFilters(newSelected);
  };

  return (
    <>
      <div className="flex flex-col md:hidden">
        <button
          className="my-2.5 justify-between text-primary-helmholtz-dunkelblau hover:text-primary-helmholtz-hellblau hover:underline hover:border-primary-helmholtz-hellblau border-2 rounded-full border-secondary-helmholtz-mint"
          onClick={() => setIsOpen(true)}
        >
          <div className="text-2xl font-semibold py-2">
            {t("filter_title", { count: Object.keys(filtersPerName).length })}
          </div>
        </button>
        <Dialog
          open={isOpen}
          onClose={() => setIsOpen(false)}
          className="relative z-50 overflow-y-scroll"
        >
          <div className="fixed inset-0 flex w-screen items-center justify-center p-4">
            <Dialog.Panel className="max-w-lg space-y-4 border bg-whitesmoke p-12 rounded-lg">
              {Object.keys(filtersPerName).map((filterName) => {
                return (
                  <MultiCheckFilter
                    key={`${filterName}`}
                    title={s(filterName)}
                    availableEntities={filtersPerName[filterName]}
                    selectedEntities={selectedFilters}
                    toggleFilter={toggleFilter}
                    unselectAll={() => unselectFilterByFilerName(filterName)}
                  />
                );
              })}
              <div className="flex gap-4 justify-center rounded-full py-1 text-primary-helmholtz-dunkelblau border-2 border-secondary-helmholtz-mint hover:text-primary-helmholtz-hellblau hover:border-primary-helmholtz-hellblau hover:underline">
                <button onClick={() => setIsOpen(false)}>{t("close")}</button>
              </div>
            </Dialog.Panel>
          </div>
        </Dialog>
      </div>

      <div className="hidden md:flex flex-col">
        <div className="md:mb-10 text-2xl font-semibold my-5">
          {t("filter_title", { count: Object.keys(filtersPerName).length })}
        </div>
        {Object.keys(filtersPerName).map((filterName) => {
          return (
            <MultiCheckFilter
              key={`${filterName}`}
              title={s(filterName)}
              availableEntities={filtersPerName[filterName]}
              selectedEntities={selectedFilters}
              toggleFilter={toggleFilter}
              unselectAll={() => unselectFilterByFilerName(filterName)}
            />
          );
        })}
      </div>
    </>
  );
};

export default Filters;
