import Image from "next/image";
import { useTranslations } from "next-intl";
import React, { useEffect, useRef, useState } from "react";

import { INSTITUTE_ROR_LOGOS } from "@/app/api/config/config";
import { Link } from "@/i18n/routing";
import ExternalLinkIcon from "@/resources/images/svg/ExternalLinkIcon";

type Props = {
  filteredArr: { label: string; type: string; id: string }[];
  type: "Parent" | "Child" | "Related";
};

const Relationships = ({ filteredArr, type }: Props) => {
  const t = useTranslations("ResultItem");
  const s = useTranslations("BackendIds");
  const relRef = useRef<HTMLParagraphElement>(null);
  const [relShowMore, setRelShowMore] = useState(false);
  const [relShowReadMore, setRelShowReadMore] = useState(false);

  useEffect(() => {
    const element = relRef.current;
    if (element && element.offsetHeight && element.scrollHeight) {
      if (
        element.offsetHeight < element.scrollHeight ||
        element.offsetWidth < element.scrollWidth
      ) {
        setRelShowReadMore(true);
      } else {
        setRelShowReadMore(false);
      }
    }
  }, []);
  return (
    <div className="mt-2 cursor-default">
      <span className="font-semibold">{s(`${type.toLowerCase()}`)}: </span>

      <p ref={relRef} className={relShowMore ? "" : "line-clamp-2 "}>
        {filteredArr.map((displayItem, idx: number) => {
          const iconArr = INSTITUTE_ROR_LOGOS.filter((o) => o.id === displayItem.id);
          const iconHref = iconArr.pop()?.logo || "";

          return (
            <span key={displayItem.id} className="">
              <Link
                href={displayItem.id}
                rel="noreferrer"
                target="_blank"
                className="mx-0.5 leading-loose inline-flex gap-x-0.5 align-middle items-center hover:underline hover:decoration-solid hover:decoration-primary-helmholtz-hellblau hover:text-primary-helmholtz-hellblau visited:underline visited:decoration-solid visited:decoration-primary-helmholtz-dunkelblau visited:text-primary-helmholtz-dunkelblau"
              >
                {iconHref ? (
                  <Image
                    src={iconHref}
                    className="object-contain w-5 h-5 mx-0.5 my-1"
                    alt="Institute icon"
                    width={16}
                    height={16}
                  />
                ) : null}
                {displayItem.label}
                <ExternalLinkIcon />
              </Link>
              {idx === filteredArr.length - 1 ? null : `, `}
            </span>
          );
        })}
      </p>

      {relShowReadMore ? (
        <button
          className="py-0.75 float-right rounded-full border-[1px] border-secondary-helmholtz-mint px-2 text-xs hover:border-primary-helmholtz-hellblau"
          onClick={() => setRelShowMore(!relShowMore)}
        >
          {relShowMore ? t("show_less") : t("show_more")}
        </button>
      ) : null}
    </div>
  );
};

export default Relationships;
