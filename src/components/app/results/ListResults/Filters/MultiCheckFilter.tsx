import { Disclosure } from "@headlessui/react";
import { useTranslations } from "next-intl";
import { useEffect, useState } from "react";

import { FilterEntity } from "@/types/types";

type Props = {
  title: string;
  availableEntities: FilterEntity[];
  selectedEntities: FilterEntity[];
  toggleFilter: (entity: FilterEntity) => void;
  unselectAll: () => void;
};

const MultiCheckFilter = ({
  title,
  availableEntities,
  selectedEntities,
  toggleFilter,
  unselectAll,
}: Props) => {
  const t = useTranslations("Filters");
  const sortEntites = (a: FilterEntity, b: FilterEntity) => a.itemName.localeCompare(b.itemName);
  const [entities, setEntities] = useState<FilterEntity[]>([]);
  const [isDisabled, setDisabled] = useState(true);
  const entityInSelectedEntities = (entity: FilterEntity) =>
    selectedEntities.findIndex((selected) => selected.itemName === entity.itemName) > -1;

  const filterSearch = (event: any) =>
    setEntities(availableEntities.filter((entity) => entity.itemName.includes(event.target.value)));

  const filterData = (entity: FilterEntity) => {
    setDisabled(true);
    toggleFilter(entity);
  };

  useEffect(() => {
    setEntities(availableEntities.sort(sortEntites));
    setDisabled(false);
  }, [availableEntities]);

  return (
    availableEntities.length !== 0 && (
      <div className="flex flex-wrap flex-col py-5">
        <Disclosure>
          <Disclosure.Button className="flex justify-between p-3">
            <div className="overflow-hidden text-ellipsis font-semibold">{title}</div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              className="h-5 w-5 ui-open:rotate-180 ui-open:transform"
            >
              <path
                fillRule="evenodd"
                d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                clipRule="evenodd"
              />
            </svg>
          </Disclosure.Button>
          <Disclosure.Panel className="grid-row-3 grid-col-1 grid gap-y-1">
            <input
              className="mx-auto my-2 w-10/12 text-ellipsis rounded-full border-2 border-primary-helmholtz-dunkelblau bg-secondary-juelich-lightgrey px-4 py-2 text-xs placeholder:text-xs hover:border-primary-helmholtz-hellblau focus:border-primary-helmholtz-hellblau focus:outline-none"
              placeholder={`search for ${title}`}
              onInput={filterSearch}
            />
            <div className="flex max-h-72 flex-col content-start overflow-y-scroll mr-4 text-center ">
              {entities.map((entity, idx) => (
                <label
                  key={idx}
                  htmlFor={`${entity.itemName}`}
                  className="group relative my-1 flex cursor-pointer"
                >
                  <input
                    id={`${entity.itemName}`}
                    type="checkbox"
                    name={entity.itemName}
                    checked={entityInSelectedEntities(entity)}
                    disabled={isDisabled}
                    onChange={() => filterData(entity)}
                    className="peer group-hover:border-primary-helmholtz-hellblau mr-2 mt-0.5 h-5 min-w-5 max-w-5 appearance-none rounded-md border-2 border-primary-helmholtz-dunkelblau"
                  />
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={3}
                    stroke="currentColor"
                    className="absolute left-0.5 top-1 h-4 w-4 rounded-md text-primary-helmholtz-hellblau text-opacity-0 peer-checked:text-opacity-100"
                  >
                    <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12.75l6 6 9-13.5" />
                  </svg>
                  <span
                    className="overflow-hidden text-ellipsis text-nowrap text-left lg:basis-[90%] 2xl:basis-[94%]"
                    title={`${entity.itemName} (${entity.itemCount})`}
                  >
                    {entity.itemName}
                    <span className="text-xs">{` (${entity.itemCount})`}</span>
                  </span>
                </label>
              ))}
              {entities.length === 0 && `No results for this search`}
            </div>
            {selectedEntities.length > 0 && (
              <button
                hidden={false}
                onClick={unselectAll}
                className="float-right mt-2 w-full justify-end justify-self-center rounded-full border-2 border-secondary-helmholtz-highlightblau py-0.5 text-xs font-normal hover:border-primary-helmholtz-hellblau hover:text-primary-helmholtz-hellblau/75"
              >
                {t("clear_all")}
              </button>
            )}
          </Disclosure.Panel>
        </Disclosure>
      </div>
    )
  );
};

export default MultiCheckFilter;
