import Image from "next/image";
import { useTranslations } from "next-intl";
import { useEffect, useRef, useState } from "react";

import { INSTITUTE_ROR_LOGOS } from "@/app/api/config/config";
import ExternalLink from "@/components/ExternalLink";
import Relationships from "@/components/app/results/ListResults/ResultItem/Relationships";
import { Link } from "@/i18n/routing";
import ExternalLinkIcon from "@/resources/images/svg/ExternalLinkIcon";
import type { ResultItem as ResultItemType } from "@/types/types";

type Props = {
  item: ResultItemType;
  idx: string;
};

const ResultItem = ({ item, idx }: Props) => {
  const t = useTranslations("ResultItem");
  const s = useTranslations("BackendIds");
  const descRef = useRef<HTMLParagraphElement>(null);
  const [descShowMore, setDescShowMore] = useState(false);
  const [descShowReadMore, setDescShowReadMore] = useState(false);
  const [titleIconHref, setTitleIconHref] = useState<string>("");

  useEffect(() => {
    const element = descRef.current;
    if (element && element.offsetHeight && element.scrollHeight) {
      if (
        element.offsetHeight < element.scrollHeight ||
        element.offsetWidth < element.scrollWidth
      ) {
        setDescShowReadMore(true);
      } else {
        setDescShowReadMore(false);
      }
    }

    const iconArr = INSTITUTE_ROR_LOGOS.filter((o) => o.id === item.id);
    setTitleIconHref(iconArr.pop()?.logo || "");
  }, [item]);

  const displayTextItem = (displayItem: string | string[]) =>
    Array.isArray(displayItem) ? displayItem.join(", ") : displayItem;

  const renderLinkItem = (url: string, isEmail = false) =>
    isExternalLink(url) ? (
      <Link
        href={isEmail ? `mailto:${url}` : `${url}`}
        rel="noreferrer"
        target={isEmail ? "_self" : "_blank"}
        className="inline-flex gap-x-0.5 text-wrap hover:underline hover:decoration-solid hover:decoration-primary-helmholtz-hellblau hover:text-primary-helmholtz-hellblau visited:underline visited:decoration-solid visited:decoration-primary-helmholtz-dunkelblau visited:text-primary-helmholtz-dunkelblau h-3"
      >
        {url}
        <ExternalLinkIcon />
      </Link>
    ) : (
      <span className="cursor-default">{url}</span>
    );

  const displayLinkItem = (displayItem: string | string[], isEmail = false) =>
    Array.isArray(displayItem)
      ? displayItem.map((url, idx) => (
          <span key={idx}>
            {renderLinkItem(url, isEmail)}
            {idx === displayItem.length - 1 ? null : `, `}
          </span>
        ))
      : renderLinkItem(displayItem, isEmail);

  const displayKeywords = (displayItem: string | string[]) => {
    return displayItem
      .toString()
      .replaceAll(";", ",")
      .split(",")
      .map((keyword, idx) => (
        <span
          key={`${idx}_${keyword}`}
          className="rounded-full border-2 border-tertiary-helmholtz-leuchtorange/30 px-2 py-1 text-md"
        >
          {keyword}
        </span>
      ));
  };

  const isExternalLink = (data: string) =>
    data.startsWith("https://") || data.startsWith("http://");

  const displayRelationships = (
    displayItems: { label: string; type: string; id: string }[],
    type: "Parent" | "Child" | "Related"
  ) => {
    const filteredArr = displayItems.filter((displayItem) => displayItem.type === type);
    return filteredArr.length > 0 ? <Relationships filteredArr={filteredArr} type={type} /> : null;
  };

  return (
    <div
      key={idx}
      className="my-2 space-y-2 rounded-xl border-2 border-primary-helmholtz-hellblau/20 px-5 py-5 shadow-lg shadow-secondary-helmholtz-highlightblau bg-white"
    >
      <div className="inline-flex align-middle items-center pb-2">
        {titleIconHref ? (
          <span className="mr-1">
            <Image
              src={titleIconHref}
              className="object-contain w-5 h-5"
              alt="Institute icon"
              width={16}
              height={16}
            />
          </span>
        ) : null}
        <div className="font-semibold" rel="noreferrer">
          {item.name}
        </div>
      </div>
      <div className="text-sm">
        <span className="font-semibold">{s("txt_identifier")}: </span>
        {displayLinkItem(item.txt_identifier ?? item.id)}
      </div>

      {item.txt_sameAs ? (
        <div className="text-sm">
          <span className="font-semibold">{s("txt_sameAs")}: </span>
          {displayLinkItem(item.txt_sameAs)}
        </div>
      ) : null}
      {item.txt_email ? (
        <div className="text-sm">
          <span className="font-semibold">{s("txt_email")}: </span>
          {displayLinkItem(item.txt_email, true)}
        </div>
      ) : null}
      {item.txt_affiliation ? (
        <div className="text-sm">
          <span className="font-semibold">{s("txt_affiliation")}: </span>
          {displayTextItem(item.txt_affiliation)}
        </div>
      ) : null}
      {item.txt_author ? (
        <div
          className="text-sm truncate max-h-full cursor-default"
          title={displayTextItem(item.txt_author)}
        >
          <span className="font-semibold">{s("txt_author")}: </span>
          {displayTextItem(item.txt_author)}
        </div>
      ) : null}
      {item.txt_creator ? (
        <div
          className="text-sm truncate max-h-full cursor-default"
          title={displayTextItem(item.txt_creator)}
        >
          <span className="font-semibold">{s("txt_creator")}: </span>
          {displayTextItem(item.txt_creator)}
        </div>
      ) : null}
      {item.txt_contributor ? (
        <div
          className="text-sm truncate max-h-full cursor-default"
          title={displayTextItem(item.txt_contributor)}
        >
          <span className="font-semibold">{s("txt_contributor")}: </span>
          {displayTextItem(item.txt_contributor)}
        </div>
      ) : null}
      {item.txt_version ? (
        <div className="text-sm cursor-default">
          <span className="font-semibold">{s("txt_version")}: </span>
          {displayTextItem(item.txt_version)}
        </div>
      ) : null}
      {item.txt_license ? (
        <div className="text-sm">
          <span className="font-semibold">{s("txt_license")}: </span>
          {displayLinkItem(item.txt_license)}
        </div>
      ) : null}
      {item.txt_url ? (
        <div className="text-sm">
          <span className="font-semibold">{s("txt_url")}: </span>
          {displayLinkItem(item.txt_url)}
        </div>
      ) : null}
      {item.txt_codeRepository ? (
        <div className="text-sm">
          <span className="font-semibold">{s("txt_codeRepository")}: </span>
          {displayLinkItem(item.txt_codeRepository)}
        </div>
      ) : null}
      {item.txt_provider ? (
        <div className="text-sm cursor-default">
          <span className="font-semibold">{s("txt_provider")}: </span>
          {displayTextItem(item.txt_provider)}
        </div>
      ) : null}
      {item.txt_publisher ? (
        <div className="text-sm cursor-default">
          <span className="font-semibold">{s("txt_publisher")}: </span>
          {displayTextItem(item.txt_publisher)}
        </div>
      ) : null}
      {item.txt_includedInDataCatalog ? (
        <div className="text-sm cursor-default">
          <span className="font-semibold">{s("txt_includedInDataCatalog")}: </span>
          {displayTextItem(item.txt_includedInDataCatalog)}
        </div>
      ) : null}
      {item.txt_keywords ? (
        <div className="flex flex-wrap space-x-1 space-y-1 text-sm cursor-default">
          <span className="pt-3 font-semibold">{s("txt_keywords")}: </span>
          {displayKeywords(item.txt_keywords)}
        </div>
      ) : null}
      {item.description ? (
        <div className="text-sm">
          <span className="font-semibold">{s("description")}: </span>
          <p
            ref={descRef}
            className={descShowMore ? "cursor-default" : "line-clamp-2 cursor-default"}
          >
            {item.description}
          </p>
          {descShowReadMore ? (
            <button
              className="py-0.75  float-right rounded-full border-[1px] border-secondary-helmholtz-mint px-2 text-xs hover:border-primary-helmholtz-hellblau"
              onClick={() => setDescShowMore(!descShowMore)}
            >
              {descShowMore ? t("show_less") : t("show_more")}
            </button>
          ) : null}
        </div>
      ) : null}

      <>
        {item.email_address ? (
          <div className="text-sm">
            <span className="font-semibold">{s("txt_email")}: </span>
            {displayLinkItem(item.email_address)}
          </div>
        ) : null}
        {item.links ? (
          <div className="text-sm">
            <span className="font-semibold">{s("links")}: </span>
            {displayLinkItem(item.links)}
          </div>
        ) : null}
        {item.status ? (
          <div className="text-sm capitalize">
            <span className="font-semibold">{s("status")}: </span>
            {displayTextItem(item.status)}
          </div>
        ) : null}
        {item.established ? (
          <div className="text-sm">
            <span className="font-semibold">{s("established")}: </span>
            {displayTextItem(item.established.toString())}
          </div>
        ) : null}
        {item.acronyms && item.acronyms.length > 0 ? (
          <div className="text-sm">
            <span className="font-semibold">{s("acronyms")}: </span>
            {displayTextItem(item.acronyms)}
          </div>
        ) : null}
        {item.types && item.types.length > 0 ? (
          <div className="text-sm">
            <span className="font-semibold">{s("types")}: </span>
            {displayTextItem(item.types)}
          </div>
        ) : null}
        {item.wikipedia_url ? (
          <div className="text-sm">
            <span className="font-semibold">{s("wikipedia_url")}: </span>
            {displayLinkItem(item.wikipedia_url)}
          </div>
        ) : null}
        {item.relationships && item.relationships.length > 0 ? (
          <div className="text-sm ">
            <div className="font-semibold">{s("relationships")}:</div>
            <div className="flex flex-col pl-8 text-sm">
              {displayRelationships(item.relationships, "Parent")}
              {displayRelationships(item.relationships, "Child")}
              {displayRelationships(item.relationships, "Related")}
            </div>
          </div>
        ) : null}
      </>

      <div className="place-content-center pt-6">
        <ExternalLink
          key={item.id}
          href={`/api/source?id=${encodeURIComponent(item.id)}`}
          className="text-xs hover:underline"
        >
          <span className="inline-flex gap-x-1 h-3">
            {t("view_source")}
            <ExternalLinkIcon />
          </span>
        </ExternalLink>
      </div>
    </div>
  );
};
export default ResultItem;
