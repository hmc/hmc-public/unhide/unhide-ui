import ReactPaginate from "react-paginate";

type Props = {
  pageCount: number;
  currentPage: number;
  onPageChange: (newStart: number) => void;
};

const Pagination = ({ pageCount, currentPage, onPageChange }: Props) => {
  return (
    <ReactPaginate
      breakLabel="..."
      nextLabel=" >"
      onPageChange={(event) => onPageChange(event.selected)}
      pageRangeDisplayed={3}
      pageCount={Math.ceil(pageCount)}
      forcePage={currentPage}
      previousLabel="< "
      renderOnZeroPageCount={null}
      className="flex flex-wrap place-content-center space-x-7"
      activeLinkClassName="font-bold underline"
    />
  );
};

export default Pagination;
