import { useTranslations } from "next-intl";

import FilterTag from "@/components/app/results/ListResults/SelectedFiltersBar/FilterTag";
import type { FilterEntity } from "@/types/types";

type Props = {
  selectedFilters: FilterEntity[];
  setSelectedFilters: (newFilters: FilterEntity[]) => void;
};

const SelectedFiltersBar = ({ selectedFilters, setSelectedFilters }: Props) => {
  const t = useTranslations("Filters");
  const removeFilterEntity = (entity: FilterEntity) => {
    setSelectedFilters(
      selectedFilters.filter(
        (selected) =>
          !(selected.filterName === entity.filterName && selected.itemName === entity.itemName)
      )
    );
  };

  return (
    <div
      className={`${selectedFilters.length !== 0 ? "grid grid-cols-5 pt-2 pb-5 gap-x-4" : "hidden"}`}
    >
      <div className="col-span-3 flex flex-wrap gap-y-1">
        {selectedFilters.map((entity, ind2) => (
          <FilterTag key={ind2} filterEntity={entity} handleRemove={removeFilterEntity} />
        ))}
      </div>
      <div className="col-span-2 flex place-content-end">
        {selectedFilters.length !== 0 && (
          <button
            onClick={() => setSelectedFilters([])}
            className="h-max cursor-pointer rounded-full border-2 border-secondary-helmholtz-mint bg-secondary-helmholtz-mint px-3 py-1 hover:border-primary-helmholtz-hellblau hover:bg-primary-helmholtz-hellblau"
          >
            {t("clear_filters")}
          </button>
        )}
      </div>
    </div>
  );
};

export default SelectedFiltersBar;
