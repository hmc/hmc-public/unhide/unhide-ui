import { useTranslations } from "next-intl";

import type { FilterEntity } from "@/types/types";

type Props = {
  filterEntity: FilterEntity;
  handleRemove: (filterEntity: FilterEntity) => void;
};

const FilterTag = ({ filterEntity, handleRemove }: Props) => {
  const t = useTranslations("BackendIds");
  return (
    <div className="flex max-w-25 hover:bg-secondary-helmholtz-mint items-center place-content-baseline space-x-2 divide-x-2 rounded-full border-2 border-secondary-helmholtz-mint/40 text-sm pl-3 mr-2">
      <div>{t(filterEntity.filterName) + ": " + filterEntity.itemName}</div>
      <button onClick={() => handleRemove(filterEntity)}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="mr-1 h-5 w-5 stroke-primary-helmholtz-dunkelblau"
        >
          <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
        </svg>
      </button>
    </div>
  );
};

export default FilterTag;
