import { useSearchParams } from "next/navigation";
import { useTranslations } from "next-intl";
import React, { Suspense, useEffect, useState } from "react";

import Filters from "@/components/app/results/ListResults/Filters";
import NoResults from "@/components/app/results/ListResults/NoResults";
import Pagination from "@/components/app/results/ListResults/Pagination";
import ResultItem from "@/components/app/results/ListResults/ResultItem";
import SelectedFiltersBar from "@/components/app/results/ListResults/SelectedFiltersBar";
import Error from "@/components/layout/Error";
import Spinner from "@/components/layout/Spinner";
import type { Category, FilterEntity, ResultItem as ResultItemType } from "@/types/types";
import { search, getRorInfo } from "@/utils/apiCall";

type Props = {
  activeCategory: Category;
};

const ListResultsComponent = ({ activeCategory }: Props) => {
  const t = useTranslations("ListResults");
  const s = useTranslations("Categories");
  const resultParams = useSearchParams();
  const [dataLoading, setDataLoading] = useState(true);
  const [hasError, setHasError] = useState(false);
  const [start, setStart] = useState(0);
  const [results, setResults] = useState<ResultItemType[]>([]);
  const [searchCount, setSearchCount] = useState(0);
  const [availableFilters, setAvailableFilters] = useState<FilterEntity[]>([]);
  const [selectedFilters, setSelectedFilters] = useState<FilterEntity[]>([]);
  const searchText = resultParams.get("searchText") ?? undefined;
  // define this here to make it possible to change later
  const resultsPerPage = 10;

  const setPage = (newPage: number) => {
    setStart(newPage * resultsPerPage);
  };

  const changeFilter = (newFilters: FilterEntity[]) => {
    setPage(0);
    setSelectedFilters(newFilters);
  };

  const getEnrichedRecords = async (rorRecords: ResultItemType[]): Promise<ResultItemType[]> => {
    const withEnrichedRecords: ResultItemType[] = [];
    for (const record of rorRecords) {
      const rorId = record.id.startsWith("https://ror.org") ? record.id.split("/").pop() : null;

      try {
        withEnrichedRecords.push(rorId ? { ...record, ...(await getRorInfo(rorId)) } : record);
      } catch (error) {
        console.error("Error while enriching record:", error);
        withEnrichedRecords.push(record);
      }
    }

    return withEnrichedRecords;
  };

  useEffect(() => {
    const updateResultList = async () => {
      setDataLoading(true);
      try {
        const searchResult = await search(
          activeCategory?.id,
          searchText,
          resultsPerPage,
          start,
          selectedFilters
        );

        // handling enrichment of institutions records
        if (activeCategory?.id === "institutions") {
          const enrichedRecords = await getEnrichedRecords(searchResult.records);
          setResults(enrichedRecords);
        } else {
          setResults(searchResult.records);
        }

        setSearchCount(searchResult.totalCount);
        setAvailableFilters(searchResult.filters);
      } catch (error) {
        console.log("Error on updating result list", error);
        setHasError(true);
      } finally {
        setDataLoading(false);
      }
    };

    updateResultList().catch(console.error);
  }, [activeCategory, searchText, start, resultsPerPage, selectedFilters]);

  return (
    <>
      {hasError ? (
        <Error />
      ) : (
        <div
          className={`${
            availableFilters.length !== 0
              ? "md:divide-x-2 md:divide-primary-helmholtz-hellblau"
              : ""
          } md:grid md:grid-cols-12 md:min-h-full`}
        >
          <div
            className={`${
              availableFilters.length !== 0 ? "md:col-span-4 lg:col-span-3 min-h-full" : "hidden"
            }  flex flex-col px-4 xl:px-6`}
          >
            <Filters
              availableFilters={availableFilters}
              selectedFilters={selectedFilters}
              setSelectedFilters={changeFilter}
            />
          </div>

          <div
            className={`${
              availableFilters.length !== 0
                ? "md:col-start-5 md:col-end-13 lg:col-start-4 lg:col-end-13"
                : "col-span-12"
            } flex flex-col md:pl-8 md:pb-5 m-6 md:mb-8`}
          >
            <div className="py-5 text-primary-helmholtz-dunkelblau underline">
              {t("result_count", {
                count: activeCategory.count,
                category: s(activeCategory.id, { count: activeCategory.count }),
              })}
            </div>
            <SelectedFiltersBar
              selectedFilters={selectedFilters}
              setSelectedFilters={changeFilter}
            />
            <Pagination
              currentPage={start / 10}
              pageCount={searchCount / resultsPerPage}
              onPageChange={setPage}
            />
            {dataLoading ? (
              <Spinner />
            ) : (
              <div className="flex flex-col py-5 xl:py-8">
                {results.length ? (
                  results.map((item: ResultItemType) => (
                    <ResultItem key={item.id} item={item} idx={item.id} />
                  ))
                ) : (
                  <NoResults />
                )}
              </div>
            )}
            <Pagination
              currentPage={start / 10}
              pageCount={searchCount / resultsPerPage}
              onPageChange={setPage}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default function ListResults(props: Props) {
  return (
    <Suspense>
      <ListResultsComponent {...props} />
    </Suspense>
  );
}
