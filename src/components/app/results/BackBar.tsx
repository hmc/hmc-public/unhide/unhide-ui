import { useRouter } from "next/navigation";
import React from "react";

const BackBar = () => {
  const router = useRouter();

  return (
    <div className="p-4 md:mx-0 md:mt-0">
      <svg
        onClick={() => router.back()}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        strokeWidth={1.5}
        stroke="currentColor"
        className="h-6 w-6 duration-300 hover:-translate-x-2 hover:stroke-primary-helmholtz-hellblau cursor-pointer"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"
        />
      </svg>
    </div>
  );
};
export default BackBar;
