import { useSearchParams } from "next/navigation";
import { useTranslations } from "next-intl";
import { Dispatch, SetStateAction, Suspense } from "react";

import { Link } from "@/i18n/routing";
import type { Category } from "@/types/types";

type Props = {
  categories: Category[];
  category?: Category;
  setCategory: Dispatch<SetStateAction<Category | undefined>>;
};

const CategoryBarComponent = ({ categories, category, setCategory }: Props) => {
  const c = useTranslations("Categories");
  const resultParams = useSearchParams();
  const searchText = resultParams.get("searchText");
  const formatter = Intl.NumberFormat("en", { notation: "compact" });
  const search = searchText ? `&searchText=${searchText}` : "";

  return (
    <div className="bg-primary-helmholtz-dunkelblau text-primary-helmholtz-weiss grid grid-cols-2 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 2xl:flex gap-y-2 gap-x-2 xl:gap-x-6 px-4 pt-2 pb-3 md:px-10 lg:px-16 xl:pt-4 xl:min-h-24">
      {categories.map((o, idx) => (
        <Link
          href={`/results?category=${o.id}${search}`}
          shallow
          onClick={() => setCategory(o)}
          key={idx}
          className={`${
            o.id === category?.id
              ? "font-semibold underline decoration-secondary-helmholtz-mint underline-offset-[20px]"
              : "text-blue-100 hover:rounded-full hover:bg-white/[0.12] hover:font-medium hover:py-3 hover:px-2"
          } flex place-content-center cursor-pointer text-md w-full flex-1 py-2 font-normal text-primary-helmholtz-weiss `}
        >
          <div className="xl:basis-1/4 2xl:basis-1/5 mr-2 ">
            {/* count indicator for category */}
            <span className="inline-flex h-11 w-11 place-content-center rounded-full bg-primary-helmholtz-hellblau object-contain align-middle">
              <span className="self-center font-medium text-primary-helmholtz-weiss">
                {formatter.format(o.count)}
              </span>
            </span>
          </div>
          <div className="basis-2/3 self-center text-left">{c(o.id, { count: o.count })}</div>
        </Link>
      ))}
    </div>
  );
};

export default function CategoryBar(props: Props) {
  return (
    <Suspense>
      <CategoryBarComponent {...props} />
    </Suspense>
  );
}
