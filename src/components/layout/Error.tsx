import Image from "next/image";
import React from "react";

import ErrorWarning from "@/resources/images/misc/error-warning.png";
const Error = () => {
  return (
    <div className="grow flex flex-col justify-center items-center p-10 ">
      {/* TODO: change the font on the icon */}
      <Image
        src={ErrorWarning || ""}
        alt="image to indicate something went wrong"
        className="max-h-60 max-w-60"
      />
      {/* <p className="text-center text-tertiary-helmholtz-leuchtorange">Please try again later!</p> */}
    </div>
  );
};

export default Error;
