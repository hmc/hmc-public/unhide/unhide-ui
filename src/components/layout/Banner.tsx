import { useTranslations } from "next-intl";

const Banner = () => {
  const t = useTranslations("Banner");
  return (
    <div className="flex px-20 py-2 mb-5 border-tertiary-helmholtz-leuchtorange border-2 rounded-full bg-tertiary-helmholtz-leuchtorange text-white justify-center cursor-default">
      <span className="font-semibold whitespace-pre mr-4">{t("title")}</span>
      <div>{t("text")}</div>
    </div>
  );
};

export default Banner;
