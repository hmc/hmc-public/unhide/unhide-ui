import Image from "next/image";
import { useTranslations } from "next-intl";

import Search from "@/components/layout/Search";
import { Link } from "@/i18n/routing";
import logoHGF from "@/resources/images/logo/Logo_HGF-KG_text_brightback.png";
import unhideLogo from "@/resources/images/logo/unhide_header.png";

const Header = () => {
  const t = useTranslations("Search");
  const s = useTranslations("Intro");
  return (
    <div className="flex items-center justify-between px-4 md:px-12 lg:px-16">
      <div className=" basis-1/3 md:basis-1/8">
        {/* logo */}
        <div className="">
          <Link href="/">
            <Image
              className="max-w-36 md:max-w-56 lg:max-w-64 xl:max-w-72"
              src={unhideLogo}
              alt="UnHide Logo"
              priority
            />
          </Link>
        </div>
      </div>

      {/* secondary nav */}
      <div className="hidden lg:flex lg:basis-1/2 xl:basis-2/3 ">
        <div className="mx-auto flex grow items-center justify-end">
          <Search exampleTrigger={t("try")} placeholder={t("placeholder")} />
        </div>
      </div>

      <div className="flex text-xs text-primary-helmholtz-hellblau items-center lg:hidden">
        {s("initiative_of")}
        <Image
          src={logoHGF}
          className="max-w-20 max-h-20 md:max-w-28 md:max-h-28"
          alt="Helmholtz KG Logo"
        />
      </div>
    </div>
  );
};

export default Header;
