import { useTranslations } from "next-intl";
import React from "react";

import Search from "@/components/layout/Search";
import bg from "@/resources/images/misc/unhide_kg_tbg.png";

const MobileSearch = () => {
  const t = useTranslations("Search");
  return (
    <div
      id="smallScreenSearch"
      className="flex flex-col justify-center bg-blend-lighten"
      style={{
        backgroundImage: `url(${bg.src})`,
      }}
    >
      <div className="text-xl md:text-2xl text-primary-helmholtz-dunkelblau">{t("search")}</div>
      <div className="">
        {/* search bar */}
        <Search exampleTrigger={t("try")} placeholder={t("placeholder")} />
      </div>
    </div>
  );
};

export default MobileSearch;
