"use client";

import { useSearchParams, useRouter } from "next/navigation";
import { Suspense, useEffect, useState } from "react";

import { Link } from "@/i18n/routing";
import ClearIcon from "@/resources/images/svg/ClearIcon";
import RightArrowIcon from "@/resources/images/svg/RightArrowIcon";
import SearchIcon from "@/resources/images/svg/SearchIcon";

type Props = {
  exampleTrigger: string;
  placeholder: string;
};

const SearchComponent = ({ exampleTrigger, placeholder }: Props) => {
  const resultParams = useSearchParams();
  const router = useRouter();

  const [searchQuery, setSearchQuery] = useState(resultParams.get("searchText") ?? "");
  const category = resultParams.get("category") ?? "documents";

  // not really random but randomly enough for us…
  const [exampleSearch, setExamples] = useState<string[]>([]);

  useEffect(() => {
    setSearchQuery(resultParams.get("searchText") ?? "");
  }, [resultParams]);

  useEffect(() => {
    setExamples(
      [
        "GEOMAR",
        "AWI",
        "Forschungszentrum Jülich",
        "Jülich Data",
        "Climate",
        "DESY",
        "HZB",
        "KIT",
        "HZDR",
        "Rodare",
        "Pangaea",
        "Datacite",
        "Geofon",
      ]
        .sort(() => 0.5 - Math.random())
        .slice(0, 4)
    );
  }, []);

  const submitSearch = (event: any) => {
    event.preventDefault();
    router.push(
      `/results?category=${category}` + (searchQuery ? `&searchText=${searchQuery}` : "")
    );
  };

  return (
    <div className="">
      <form
        onSubmit={(e) => submitSearch(e)}
        className="flex items-center py-1 border-b-2 border-b-primary-helmholtz-dunkelblau"
      >
        <div className="basis-1/3 md:basis-11/12 py-2">
          <input
            className="pl-2 text-primary-helmholtz-dunkelblau placeholder:italic focus:outline-none bg-transparent"
            placeholder={placeholder}
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
        </div>

        {/* search option buttons */}
        {/* TODO: refactor this into a single reusable component? */}
        <div className="flex md:basis-1/12 justify-end divide-x-2">
          {searchQuery !== "" ? (
            <Link
              className="-ml-12 pr-2 md:mt-1 xl:mt-1 flex items-center"
              onClick={() => {
                setSearchQuery("");
              }}
              href={`/results?category=${category}`}
            >
              <ClearIcon />
            </Link>
          ) : null}

          <div className="pl-2">
            <button className="float-right rounded-full bg-secondary-helmholtz-mint px-3 py-3 text-primary-helmholtz-dunkelblau shadow transition duration-300 hover:bg-primary-helmholtz-hellblau">
              <SearchIcon />
            </button>
          </div>
        </div>
      </form>

      <div className="flex md:basis-11/12 space-x-2 pl-2 pt-3 items-center">
        <div className="font-bold text-primary-helmholtz-dunkelblau pr-2">{exampleTrigger}</div>
        <div className="flex flex-wrap gap-x-8 gap-y-2 flex-row md:flex-nowrap md:gap-x-10 md:grow md:justify-between h-5">
          {exampleSearch.map((query, ix) => (
            <Link
              key={ix}
              href={`/results?category=${category}&searchText=${query}`}
              className="flex items-center text-info text-primary-helmholtz-dunkelblau hover:scale-110 hover:transition hover:delay-150 hover:translate-x-2 hover:ease-in-out hover:text-primary-helmholtz-hellblau"
            >
              <div className="basis-1">{query}</div>
              <RightArrowIcon />
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default function Search(props: Props) {
  return (
    <Suspense>
      <SearchComponent {...props} />
    </Suspense>
  );
}
