import Image from "next/image";
import Link from "next/link";
import { useTranslations } from "next-intl";

import ExternalLink from "@/components/ExternalLink";
import HMCLogoInv from "@/resources/images/logo/HMC_Logo_invert_S.png";
import BlueskyLogo from "@/resources/images/svg/BlueskyLogo";
import GitlabLogo from "@/resources/images/svg/GitlabLogo";
import HelmholtzSvg from "@/resources/images/svg/HelmholtzSvg";
import LinkedInLogo from "@/resources/images/svg/LinkedInLogo";
import MailEnvelope from "@/resources/images/svg/MailEnvelope";
import MastodonLogo from "@/resources/images/svg/MastodonLogo";
import MattermostLogo from "@/resources/images/svg/MattermostLogo";

export default function Footer() {
  const t = useTranslations("Footer");
  return (
    <>
      <div className="text-primary-helmholtz-weiss w-full flex-wrap divide-y divide-secondary-helmholtz-webblassblau bg-primary-helmholtz-dunkelblau px-20 py-5 drop-shadow">
        {/* branding row */}
        <div className="my-3 grid grid-cols-1 gap-y-10">
          {/* logo row */}
          <div id="logo-row" className="flex flex-wrap gap-y-10 md:justify-between">
            <ExternalLink href="https://www.helmholtz.de/en/">
              <HelmholtzSvg />
              <div id="logo-tagline" className="-mt-5">
                {t("helmholtz_phrase")}
              </div>
            </ExternalLink>

            <ExternalLink href="https://helmholtz-metadaten.de/en">
              <Image className="max-w-60 md:max-w-72" src={HMCLogoInv} alt="hmc logo" />
            </ExternalLink>
          </div>
          {/* social links row*/}
          <div
            id="links-row"
            className="flex flex-col gap-6 md:flex-row md:gap-4 md:justify-between"
          >
            <div
              id="social-links"
              className="grid grid-cols-3 md:max-w-64 md:gap-6 md:grid-cols-3 lg:flex"
            >
              <ExternalLink href="https://bsky.app/profile/helmholtzhmc.bsky.social">
                <BlueskyLogo />
                <span>{t("bluesky")}</span>
              </ExternalLink>
              <ExternalLink href="https://www.linkedin.com/company/helmholtz-metadata-collaboration-hmc">
                <LinkedInLogo />
                <span>{t("linkedin")}</span>
              </ExternalLink>
              <ExternalLink href="https://helmholtz.social/@helmholtz_hmc/110939025090632758">
                <MastodonLogo />
                <span>{t("mastodon")}</span>
              </ExternalLink>
            </div>
            <div
              id="contact-links"
              className="grid grid-cols-3 md:max-w-64 md:grid-cols-3 lg:gap-6 lg:flex"
            >
              <ExternalLink href="mailto:HMC@fz-juelich.de">
                <MailEnvelope />
                <span>{t("email")}</span>
              </ExternalLink>
              <ExternalLink href="https://codebase.helmholtz.cloud/hmc/hmc-public/unhide">
                <GitlabLogo />
                <span>{t("gitlab")}</span>
              </ExternalLink>
              <ExternalLink href="https://mattermost.hzdr.de/hmc-public">
                <MattermostLogo />
                <span>{t("mattermost")}</span>
              </ExternalLink>
            </div>
          </div>
        </div>

        {/* coypright row */}
        <div className="flex-wrap mx-auto my-0 flex justify-between pt-4">
          {/* important info */}
          <div className="grid grid-cols-1 gap-y-4 md:flex md:basis-2/3 md:space-x-5 text-sm">
            <Link href="/privacy-policy">{t("privacy_protection")}</Link>
            <ExternalLink href="https://www.fz-juelich.de/en/legal-notice">
              {t("legal_information")}
            </ExternalLink>
            <ExternalLink href="https://www.fz-juelich.de/en/declaration-of-accessibility">
              {t("accessibility")}
            </ExternalLink>
          </div>

          {/* copyright info */}
          <div className="pt-4 md:pt-0 md:basis-1/3 flex justify-end cursor-default text-sm">
            {t("copyright", { year: new Date().getFullYear() })}
          </div>
        </div>
      </div>
    </>
  );
}
