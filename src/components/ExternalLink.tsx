import { PropsWithChildren } from "react";

import { Link } from "@/i18n/routing";

type Props = {
  href: string;
  underlined?: boolean;
  className?: string;
};

const ExternalLink = ({ href, underlined, className, children }: PropsWithChildren<Props>) => {
  return (
    <Link
      href={href}
      target="_blank"
      rel="noreferrer"
      className={`hover:text-primary-helmholtz-hellblau ${className || ""} ${underlined ? "underline" : ""}`}
    >
      {children}
    </Link>
  );
};

export default ExternalLink;
