import { createNavigation } from "next-intl/navigation";
import { defineRouting, LocalePrefix } from "next-intl/routing";

export const i18nConfig = {
  locales: ["en", "de"],
  defaultLocale: "en",
  localeDetection: false,
  localePrefix: "as-needed" as LocalePrefix,
};
export const routing = defineRouting(i18nConfig);

export const { Link } = createNavigation(routing);
