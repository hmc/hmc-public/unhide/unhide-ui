import { type NextRequest } from "next/server";

import {
  searchSchema,
  type searchParamsType,
  type SearchResponse,
  type ResultItem,
  type SolrResponseDocsType,
  type SolrResponseType,
} from "@/types/types";
import {
  createSearchParamsMap,
  createFacetParams,
  createResultFilters,
  createResultItem,
  getSolrCategoryCountsMap,
  updateCategoryCountsForSearch,
} from "@api/utils/shared";
import SolrQuery from "@api/utils/solrQuery";

export const GET = async (req: NextRequest) => {
  try {
    const searchParamsMap = createSearchParamsMap(req.nextUrl.searchParams);
    const requestBody = searchSchema.safeParse(Object.fromEntries(searchParamsMap));

    if (!requestBody.success) {
      const { errors } = requestBody.error;
      return Response.json({ error: { message: "Invalid request", errors } }, { status: 400 });
    }

    const {
      searchText,
      start,
      rows,
      facetType,
      facetName,
      documentType,
      region,
      includeFacets,
    }: searchParamsType = requestBody.data;

    // build SOLR query, query the SOLR instance for data
    const { facetIntervalFields, queryFacetFields } = createFacetParams(documentType);
    const solrResponse: SolrResponseType = await new SolrQuery(
      start,
      rows,
      queryFacetFields,
      facetIntervalFields,
      facetType,
      facetName,
      searchText,
      documentType,
      region
    ).json();

    // create the response object in the desired shape
    const responseRecords: ResultItem[] = solrResponse.response.docs.map(
      (record: SolrResponseDocsType) => createResultItem(record)
    );

    const responseTotalCount = solrResponse.response.numFound;

    const categoryCounts = updateCategoryCountsForSearch(
      getSolrCategoryCountsMap(solrResponse.facet_counts.facet_fields.type)
    );

    const responseFilters = includeFacets
      ? createResultFilters(solrResponse.facet_counts.facet_fields)
      : [];

    const response: SearchResponse = {
      records: responseRecords,
      totalCount: responseTotalCount,
      categoryCounts,
      filters: responseFilters,
    };

    return Response.json(response);
  } catch (error) {
    console.error("Error while handling search", error);
    return Response.json(
      { error: { message: "Error while fetching search data" } },
      { status: 500 }
    );
  }
};
