import type {
  Category,
  FacetIntervalFieldKeys,
  FacetFieldKeys,
  SOLRCategories,
} from "@/types/types";

export const CATEGORIES: Category[] = [
  {
    id: "datasets",
    text: "Datasets",
    count: 0,
  },
  {
    id: "software",
    text: "Software",
    count: 0,
  },
  {
    id: "documents",
    text: "Documents",
    count: 0,
  },
  {
    id: "instruments",
    text: "Instruments",
    count: 0,
  },
  {
    id: "experts",
    text: "Experts",
    count: 0,
  },
  {
    id: "institutions",
    text: "Institutions",
    count: 0,
  },
  {
    id: "trainings",
    text: "Trainings",
    count: 0,
  },
  {
    id: "projects",
    text: "Projects",
    count: 0,
  },
];

export const INITIAL_CATECORY_COUNT_MAP: Record<FacetFieldKeys, number> = {
  experts: 0,
  documents: 0,
  datasets: 0,
  institutions: 0,
  trainings: 0,
  projects: 0,
  software: 0,
  instruments: 0,
};

export const AVAILABLE_FACETS: string[] = [
  "txt_knowsAbout",
  "txt_knowsLanguage",
  "txt_nationality",
  "txt_jobTitle",
  "txt_contributor",
  "txt_keywords",
  "txt_memberOf",
  "txt_parentOrganization",
  "id_provider",
  "id_includedInDataCatalog",
];

// Facets are what is shown for each entry in the list of the web frontend and can be filtered for
// Common is for combined types
const COMMON_FACET_FIELDS = {
  document: [
    "txt_keywords",
    "txt_provider",
    "txt_license",
    "txt_contributor",
    "dt_datePublished",
    "txt_publisher",
  ],
  software: [
    "txt_license",
    "txt_keywords",
    "txt_developmentStatus",
    "txt_programmingLanguage",
    "txt_runtimePlatform",
    "txt_publisher",
    "txt_version",
  ],
};

export const FACET_FIELDS: Record<FacetFieldKeys, string[]> = {
  experts: [
    "txt_memberOf",
    "txt_knowsLanguage",
    "txt_jobTitle",
    "txt_knowsAbout",
    "txt_affiliation",
    "txt_provider",
  ],
  institutions: ["txt_memberOf", "txt_provider"],
  datasets: [
    "txt_keywords",
    "txt_license",
    "txt_provider",
    "txt_publisher",
    "txt_variableMeasured",
  ],
  documents: COMMON_FACET_FIELDS["document"],
  trainings: ["txt_keywords", "txt_provider", "txt_author", "txt_educationalCredentialAwarded"],
  projects: ["txt_keywords", "txt_provider", "txt_areaServed"],
  software: COMMON_FACET_FIELDS["software"],
  instruments: ["txt_keywords"],
};

export const DEFAULT_FACET_FIELDS: string[] = [
  "txt_knowsAbout",
  "name",
  "txt_knowsLanguage",
  "txt_nationality",
  "txt_jobTitle",
  "txt_contributor",
  "txt_keywords",
  "txt_memberOf",
  "txt_parentOrganization",
  "id_provider",
  "has_geom",
  "id_includedInDataCatalog",
  "id_identifier",
  "id",
  "keys",
  "type",
];

export const FACET_INTERVALS: Record<FacetIntervalFieldKeys, string[]> = {
  documents: [],
  datasets: [],
};

export const COMBINED_TYPES: Record<FacetFieldKeys, SOLRCategories[]> = {
  experts: ["Person"],
  software: [
    "SoftwareSourceCode",
    "SoftwareApplication",
    "SoftwareLibrary",
    "WebApplication",
    "CommandlineApplication",
  ],
  datasets: ["Dataset"],
  documents: [
    "DigitalDocument",
    "ScholarlyArticle",
    "CreativeWork",
    "Report",
    "Book",
    "Thesis",
    "Chapter",
    "Article",
  ],
  institutions: ["Organization", "DataCatalog"],
  instruments: ["Instruments"],
  projects: ["ResearchProject"],
  trainings: ["Course", "Event"],
};

//NOTE: https://www.freecodecamp.org/news/javascript-range-create-an-array-of-numbers-with-the-from-method/
const rangedArray = (start: number, stop: number, step: number): number[] => {
  return Array.from({ length: (stop - start) / step + 1 }, (value, index) => start + index * step);
};

export const FACET_INTERVAL_DEFAULTS = ["[*,1800)", "[1800,1900)", "[1900,1950)"];

for (const x of rangedArray(1950, 2010, 10)) {
  FACET_INTERVAL_DEFAULTS.push(`[${x}, ${x + 10})`);
}
for (const x of rangedArray(2010, 2030, 2)) {
  FACET_INTERVAL_DEFAULTS.push(`[${x}, ${x + 2})`);
}

export const SOLR_QUERY_DEFAULTS = {
  rows: 10,
  start: 0,
  facetMinCount: 1,
  query: "*:*",
  sort: "score desc, indexed_ts desc",
  facet: true,
  facetStartYearLimit: 30,
  facetEndYearLimit: 30,
  qop: "AND",
  defType: "edismax",
  qf: "name^4 txt_keywords^2 text",
};

export const SOLR_RESULT_ITEM_KEYS = [
  "id",
  "name",
  "type",
  "description",
  "txt_identifier",
  "txt_keywords",
  "txt_includedInDataCatalog",
  "txt_sameAs",
  "txt_email",
  "txt_affiliation",
  "txt_author",
  "txt_creator",
  "txt_publisher",
  "txt_contributor",
  "txt_version",
  "txt_license",
  "txt_url",
  "txt_codeRepository",
  "txt_provider",
  "txt_endDate",
  "txt_location",
  "txt_startDate",
  "txt_performer",
] as const;

export const SOLR_RESULT_CONFIG_KEYS = ["index_id", "_version_", "indexed_ts"] as const;

export const SOLR_CATEGORIES = [
  "DigitalDocument",
  "Dataset",
  "Person",
  "Organization",
  "ScholarlyArticle",
  "CreativeWork",
  "DataCatalog",
  "SoftwareSourceCode",
  "Report",
  "Book",
  "SoftwareApplication",
  "Event",
  "Thesis",
  "Article",
  "SoftwareLibrary",
  "WebApplication",
  "CommandlineApplication",
  "Chapter",
  "ResearchProject",
  "Course",
  "Instruments",
] as const;

export const INSTITUTE_ROR_LOGOS = [
  // Helmholtz Association
  {
    id: "https://ror.org/0281dp749",
    logo: "https://www.helmholtz.de/typo3conf/ext/dreipc_hgf/Resources/Public/Frontend/Build/assets/icons/favicons/icon-228x228.png",
  },
  // 18 Helmholtz Centers
  {
    id: "https://ror.org/02h2x0161",
    logo: "https://www.geomar.de/typo3conf/ext/geomar_provider/Resources/Public/Graphics/favicon/favicon-16x16.png",
  },
  {
    id: "https://ror.org/03qjp1d79",
    logo: "https://hereon.de/favicon-16x16.png",
  },
  {
    id: "https://ror.org/04p5ggc03",
    logo: "https://www.mdc-berlin.de/sites/default/files/favicons/favicon-16x16.png",
  },
  {
    id: "https://ror.org/01js2sh04",
    logo: "https://www.desy.de/++resource++desy/images/desy_logo_3c_web.svg",
  },
  {
    id: "https://ror.org/032e6b942",
    logo: "https://www.awi.de/_assets/978631966794c5093250775de182779d/Images/AWI/favicon.ico",
  },
  {
    id: "https://ror.org/03d0p2685",
    logo: "https://www.helmholtz-hzi.de/_assets/ae8a1fe32f30b295243325c1db1b8058/Icons/Favicons/HZI/favicon-16x16.png",
  },
  {
    id: "https://ror.org/04z8jg394",
    logo: "https://www.gfz-potsdam.de/typo3conf/ext/gfz_www_sitepackage/Resources/Public/Icons/favicon.ico",
  },
  {
    id: "https://ror.org/02aj13c28",
    logo: "https://www.helmholtz-berlin.de/favicon.ico",
  },
  {
    id: "https://ror.org/000h6jb29",
    logo: "https://www.ufz.de/static/custom/weblayout/DefaultInternetLayout/img/favicon.ico",
  },
  {
    id: "https://ror.org/01zy2cs03",
    logo: "https://www.hzdr.de/favicon-16x16.png",
  },
  {
    id: "https://ror.org/04bwf3e34",
    logo: "https://www.dlr.de/icon.svg",
  },
  {
    id: "https://ror.org/02k8cbn47",
    logo: "https://www.gsi.de/favicon.ico",
  },
  {
    id: "https://ror.org/00cfam450",
    logo: "https://www.helmholtz-munich.de/typo3conf/ext/helmholtz_sitepackage/Resources/Public/Icons/Favicons/favicon-16x16.png",
  },
  {
    id: "https://ror.org/04t3en479",
    logo: "https://www.kit.edu/img/intern/favicon.ico",
  },
  {
    id: "https://ror.org/04cdgtt98",
    logo: "https://www.dkfz.de/global/img/favicon.ico",
  },
  {
    id: "https://ror.org/02njgxr09",
    logo: "https://cispa.de/images/cispa-logo-on-bright.svg",
  },
  {
    id: "https://ror.org/043j0f473",
    logo: "https://www.dzne.de/typo3conf/ext/fe_website/Resources/Public/AppIcons/favicon-16x16.png",
  },
  {
    id: "https://ror.org/02nv7yv05",
    logo: "https://www.fz-juelich.de/icon.svg",
  },
];
