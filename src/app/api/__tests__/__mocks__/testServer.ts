// creating a mock server that emulates nextjs' server
// solution based on final response at: https://github.com/vercel/next.js/discussions/15166

import { createServer, IncomingMessage, Server, ServerResponse } from "http";
import { type NextApiHandler } from "next";
import { apiResolver } from "next/dist/server/api-utils/node/api-resolver";

const getMockServer = (handler: NextApiHandler, bodyParser = true): Server => {
  const requestHandler = (request: IncomingMessage, response: ServerResponse<IncomingMessage>) => {
    // The searchParams object is part of the URL object.
    // we need to create it ourselves in order to be able to test urls with searchparams

    // read doc on message.url at https://nodejs.org/api/http.html#class-httpincomingmessage, to understand code from line 18-22
    const urlObj = new URL(request.url as string, `http://${request.headers.host}`);
    const paramsObj = urlObj.searchParams;

    // the searchParams object needs to be massaged into the shape we expect and get for the req.query object in next - the following does this
    const query: Record<string, string | string[]> = {};
    for (const key of paramsObj.keys()) {
      const arr = paramsObj.getAll(key);
      if (arr.length < 2) query[key] = arr[0];
      else query[key] = arr;
    }

    return apiResolver(
      request,
      response,
      query,
      Object.assign(handler, { config: { api: { bodyParser } } }),
      {
        previewModeEncryptionKey: "",
        previewModeId: "",
        previewModeSigningKey: "",
      },
      true
    );
  };

  return createServer(requestHandler);
};

export default getMockServer;
