import { NextRequest } from "next/server";

import solrWithSingleDifferentFilter from "@/app/api/__tests__/__mocks__/solrResults/10.withSingleDifferentFilters.json";
import solrWithMultipleDifferentFilter from "@/app/api/__tests__/__mocks__/solrResults/11.withMultipleDifferentFilters.json";
import solrNoParamsSearch from "@/app/api/__tests__/__mocks__/solrResults/4.noParamsSearch.json";
import solrSearchTextResponse from "@/app/api/__tests__/__mocks__/solrResults/5.onlySearchText.json";
import solrInstitutionsResponse from "@/app/api/__tests__/__mocks__/solrResults/6.onlyDocumentType.json";
import solrDocAndTextSearch from "@/app/api/__tests__/__mocks__/solrResults/7.docAndTextSearch.json";
import solrWithSingleFilter from "@/app/api/__tests__/__mocks__/solrResults/8.withSingleFilter.json";
import solrWithMulitpleSameFilter from "@/app/api/__tests__/__mocks__/solrResults/9.withMultipleSameFilters.json";
import singleInvalidParam from "@/app/api/__tests__/expectedApiResults/1.singleInvalidParam.json";
import withSingleDifferentFilters from "@/app/api/__tests__/expectedApiResults/10.withSingleDifferentFilters.json";
import withMultipleDifferentFilters from "@/app/api/__tests__/expectedApiResults/11.withMultipleDifferentFilters.json";
import multipleInvalidParams from "@/app/api/__tests__/expectedApiResults/2.multipleInvalidParams.json";
import wrongDocType from "@/app/api/__tests__/expectedApiResults/3.wrongDocType.json";
import noParamsSearch from "@/app/api/__tests__/expectedApiResults/4.noParamsSearch.json";
import searchTextApiResponse from "@/app/api/__tests__/expectedApiResults/5.onlySearchText.json";
import institutionsResponse from "@/app/api/__tests__/expectedApiResults/6.onlyDocumentType.json";
import docAndTextSearch from "@/app/api/__tests__/expectedApiResults/7.docAndTextSearch.json";
import withSingleFilter from "@/app/api/__tests__/expectedApiResults/8.withSingleFilter.json";
import withMultipleSameFilters from "@/app/api/__tests__/expectedApiResults/9.withMultipleSameFilters.json";
import { GET } from "@api/search/route";

describe("/search API Endpoint", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("1. should return a validation error from zod about number being too large", async () => {
    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("start", "0");
    req.nextUrl.searchParams.append("rows", "20");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(400);
    expect(jsonResult).toMatchObject(singleInvalidParam);
  });

  it("2. should return a validation error from zod about numbers for row being too big and start being too small", async () => {
    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("start", "-1");
    req.nextUrl.searchParams.append("rows", "20");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(400);
    expect(jsonResult).toMatchObject(multipleInvalidParams);
  });

  it("3. should return a union error for the wrong document type during validation", async () => {
    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("documentType", "foo");
    req.nextUrl.searchParams.append("rows", "0");

    const response = await GET(req);

    const jsonResult = await response.json();
    expect(response.status).toBe(400);
    expect(jsonResult).toMatchObject(wrongDocType);
  });

  it("4. should return the first 10 search results with the default values defined for params on the backend", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrNoParamsSearch),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(noParamsSearch);
  });

  it("5. should return the first 10 search results for the given search text", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrSearchTextResponse),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "julich");
    req.nextUrl.searchParams.append("start", "0");
    req.nextUrl.searchParams.append("rows", "10");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(searchTextApiResponse);
  });

  it("6. should return the first 10 records of the search with the institutions document type", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrInstitutionsResponse),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("documentType", "institutions");
    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(institutionsResponse);
  });

  it("7. should return 10 search results for the given searchtext and document type", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrDocAndTextSearch),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("documentType", "software");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(docAndTextSearch);
  });

  it("8. should return atmost 10 search result given a searchText, documentType and a filterItem", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrWithSingleFilter),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("documentType", "software");
    req.nextUrl.searchParams.append("facetType", "txt_programmingLanguage");
    req.nextUrl.searchParams.append("facetName", "JavaScript");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(withSingleFilter);
  });

  it("9. should return atmost 10 search result given a searchText, documentType and multiple filterItems of the same filterType", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrWithMulitpleSameFilter),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("documentType", "software");
    req.nextUrl.searchParams.append("facetType", "txt_programmingLanguage");
    req.nextUrl.searchParams.append("facetName", "C");
    req.nextUrl.searchParams.append("facetType", "txt_programmingLanguage");
    req.nextUrl.searchParams.append("facetName", "JavaScript");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(withMultipleSameFilters);
  });

  it("10. should return atmost 10 search result given a searchText, documentType and single filterItems of the different filterType", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrWithSingleDifferentFilter),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "API");
    req.nextUrl.searchParams.append("documentType", "software");
    req.nextUrl.searchParams.append("facetType", "txt_programmingLanguage");
    req.nextUrl.searchParams.append("facetName", "JavaScript");
    req.nextUrl.searchParams.append("facetType", "txt_runtimePlatform");
    req.nextUrl.searchParams.append("facetName", "Java");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(withSingleDifferentFilters);
  });

  it("11. should return atmost 10 search result given a searchText, documentType and multiple filterItems of the different filterType", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrWithMultipleDifferentFilter),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("searchText", "Forschungszentrum Jülich");
    req.nextUrl.searchParams.append("documentType", "documents");
    req.nextUrl.searchParams.append("facetType", "txt_contributor");
    req.nextUrl.searchParams.append("facetName", "Angst, Manuel");
    req.nextUrl.searchParams.append("facetType", "txt_contributor");
    req.nextUrl.searchParams.append("facetName", "Zorn, Reiner");
    req.nextUrl.searchParams.append("facetType", "txt_publisher");
    req.nextUrl.searchParams.append(
      "facetName",
      "Forschungszentrum Jülich GmbH, JCNS / RWTH Aachen / Univ. Münster"
    );
    req.nextUrl.searchParams.append("facetType", "txt_keywords");
    req.nextUrl.searchParams.append("facetName", "info:eu-repo/classification/ddc/600");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(withMultipleDifferentFilters);
  });
});
