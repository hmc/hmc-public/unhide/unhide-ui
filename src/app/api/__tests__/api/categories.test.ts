import { Category } from "@/types/types";
import solrCategories from "@api/__tests__/__mocks__/solrResults/categoriesApi.json";
import { GET } from "@api/categories/route";

describe("unit tests for /categories API Endpoint", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should return an success response on using the GET method", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrCategories),
      } as Response)
    );

    const response = await GET();
    const jsonResult: Category[] = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject([
      {
        id: "datasets",
        text: "Datasets",
        count: 411935,
      },
      {
        id: "software",
        text: "Software",
        count: 3521,
      },
      {
        id: "documents",
        text: "Documents",
        count: 1679342,
      },
      {
        id: "instruments",
        text: "Instruments",
        count: 0,
      },
      {
        id: "experts",
        text: "Experts",
        count: 229601,
      },
      {
        id: "institutions",
        text: "Institutions",
        count: 64472,
      },
      {
        id: "trainings",
        text: "Trainings",
        count: 667,
      },
      {
        id: "projects",
        text: "Projects",
        count: 0,
      },
    ]);
  });
});
