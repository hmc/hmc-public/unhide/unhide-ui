import { type NextResponse } from "next/server";

import { GET } from "@api/hello/route";

describe("Unit tests for /hello API Endpoint", () => {
  it("should return a hello world message", async () => {
    const response: NextResponse<{ message: string }> = GET();
    const jsonResult: { message: string } = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject({ message: "Hello from Next.js!" });
  });
});
