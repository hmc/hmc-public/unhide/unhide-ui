import { NextRequest } from "next/server";

import solrdocumentResponse from "@api/__tests__/__mocks__/solrResults/12.documentSource.json";
import solrDatasetResponse from "@api/__tests__/__mocks__/solrResults/13.datasetSource.json";
import solrWrongIdResponse from "@api/__tests__/__mocks__/solrResults/14.wrongIdSource.json";
import documentSource from "@api/__tests__/expectedApiResults/12.documentSource.json";
import datasetSource from "@api/__tests__/expectedApiResults/13.datasetSource.json";
import { GET } from "@api/source/route";

describe("unit tests for /categories API Endpoint", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should return an error when no ID is passed as a param", async () => {
    const req = new NextRequest(new Request("https://www.whatever.com"), {});

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(400);
    expect(jsonResult).toMatchObject({
      error: {
        message: "Request missing ID parameter",
      },
    });
  });

  it("should return an error on being given an empty string for id", async () => {
    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("id", "");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(400);
    expect(jsonResult).toMatchObject({
      error: {
        message: "Request missing ID parameter",
      },
    });
  });

  it("should return json source for given document ID", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrdocumentResponse),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("id", "6121f590c7bda0f1f2d1651b22871a5f");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(documentSource);
  });

  it("should return json source for given dataset ID", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrDatasetResponse),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("id", "https://doi.org/10.1594/PANGAEA.78058");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(200);
    expect(jsonResult).toMatchObject(datasetSource);
  });

  it("should return error for a faulty ID", async () => {
    jest.spyOn(global, "fetch").mockImplementationOnce(() =>
      Promise.resolve({
        json: () => Promise.resolve(solrWrongIdResponse),
      } as Response)
    );

    const req = new NextRequest(new Request("https://www.whatever.com"), {});
    req.nextUrl.searchParams.append("id", "6121f590c7bda0f1f2d1651b2271f");

    const response = await GET(req);
    const jsonResult = await response.json();
    expect(response.status).toBe(500);
    expect(jsonResult).toMatchObject({
      error: { message: "Error while fetching source for given ID" },
    });
  });
});
