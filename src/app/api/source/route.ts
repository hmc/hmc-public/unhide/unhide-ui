import { type NextRequest } from "next/server";

import { type SolrResponseType } from "@/types/types";

const SOLR_URL = new URL(process.env.NEXT_PRIVATE_SOLR_SERVICE_URL || "");

export const GET = async (req: NextRequest) => {
  const id = req.nextUrl.searchParams.get("id");
  if (!id) {
    return Response.json({ error: { message: "Request missing ID parameter" } }, { status: 400 });
  }
  try {
    const params = new URLSearchParams({
      q: "*:*",
      fq: `+id:"${id}"`,
      rows: "1",
    });

    const response = await fetch(`${SOLR_URL}/select?${params}`);
    const solrResponse: SolrResponseType = await response.json();
    return Response.json(JSON.parse(solrResponse.response.docs[0].json_source));
  } catch (error) {
    console.log("Error while handling source", error);
    return Response.json(
      { error: { message: "Error while fetching source for given ID" } },
      { status: 500 }
    );
  }
};
