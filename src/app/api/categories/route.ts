import { SOLRCategories, SOLRCountsArray, Category, SolrResponseType } from "@/types/types";
import {
  getSolrCategoryCountsMap,
  updateCategoryCounts,
  createFacetParams,
} from "@api/utils/shared";
import SolrQuery from "@api/utils/solrQuery";

export const GET = async () => {
  try {
    // Build SOLR query, query the SOLR instance for data
    const { facetIntervalFields, queryFacetFields } = createFacetParams();
    const solrResponse: SolrResponseType = await new SolrQuery(
      0,
      10,
      queryFacetFields,
      facetIntervalFields
    ).json();

    // solrResponse = SOLR_RESPONSE; // mocked response from the SOLR interaction
    const countsArray: SOLRCountsArray = solrResponse["facet_counts"]["facet_fields"]["type"];

    // massaging data into required format
    const countsMap: Record<SOLRCategories, number> = getSolrCategoryCountsMap(countsArray);
    const responseData: Category[] = updateCategoryCounts(countsMap);
    return Response.json(responseData);
  } catch (error) {
    console.error(`Error while getting category count data from SOLR`, error);
    return Response.json(
      { error: { message: "Error while fetching category counts" } },
      { status: 500 }
    );
  }
};
