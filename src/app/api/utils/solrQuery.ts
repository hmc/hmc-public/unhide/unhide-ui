import {
  COMBINED_TYPES,
  DEFAULT_FACET_FIELDS,
  FACET_INTERVAL_DEFAULTS,
  SOLR_QUERY_DEFAULTS,
} from "@/app/api/config/config";
import { FacetFieldKeys, Formats } from "@/types/types";

const SOLR_URL = new URL(process.env.NEXT_PRIVATE_SOLR_SERVICE_URL || "");

const SolrQuery = class {
  _query: SolrQueryBuilder;

  constructor(
    start: number,
    rows: number,
    facetFields: string[],
    facetIntervalFields: string[],
    facetType?: string[],
    facetName?: string[],
    searchText?: string,
    documentType?: FacetFieldKeys,
    region?: string
  ) {
    // Dismax query parser needs to have the search term in the query, not * or :.
    // So adding this to the query, and not putting it in the fq.
    // The querybuilder will filter out the *:* default here, and just run an ordinary query.
    const query = searchText ? searchText : SOLR_QUERY_DEFAULTS.query;

    const solrSearchQuery = new SolrQueryBuilder(start, query, rows);

    solrSearchQuery.addFacetFields(facetFields);

    if (documentType && Object.keys(COMBINED_TYPES).includes(documentType)) {
      solrSearchQuery.addFq("type", COMBINED_TYPES[documentType as FacetFieldKeys].join(" "));
    } else if (documentType) {
      solrSearchQuery.addFq("type", documentType);
    }

    if (region) {
      solrSearchQuery.addFq("txt_region", region);
    }

    if (facetIntervalFields && facetIntervalFields.length !== 0) {
      solrSearchQuery.addFacetInterval(facetIntervalFields, FACET_INTERVAL_DEFAULTS);
    }

    if (facetType && facetName && facetType.length !== 0 && facetType.length === facetName.length) {
      for (const idx in facetType) {
        solrSearchQuery.addFq(facetType[idx], facetName[idx]);
      }
    }

    this._query = solrSearchQuery;
  }

  async json() {
    const response = await fetch(`${SOLR_URL}/select?${this._query.params}`);
    try {
      return await response.json();
    } catch (error) {
      console.error(`Error while getting data from SOLR: ${error}`);
    }
  }
};

class SolrQueryBuilder {
  rows: string;
  facetMinCount: string;
  start: string;
  query: string;
  sort: string;
  facet: string;
  fList?: string[];
  params: URLSearchParams;

  constructor(
    start: number,
    query: string,
    rows?: number,
    facetMinCount?: number,
    sort?: string,
    facet?: boolean,
    fList?: string[]
  ) {
    // TOREVIEW: Create and use a object with all the default values below
    this.rows = (rows || SOLR_QUERY_DEFAULTS.rows).toString();
    this.start = (start || SOLR_QUERY_DEFAULTS.start).toString();
    this.facet = (facet || SOLR_QUERY_DEFAULTS.facet).toString();
    this.facetMinCount = (facetMinCount || SOLR_QUERY_DEFAULTS.facetMinCount).toString();
    this.query = query || SOLR_QUERY_DEFAULTS.query;
    this.sort = sort || SOLR_QUERY_DEFAULTS.sort;
    this.fList = fList;

    this.params = new URLSearchParams();
    this.params.append("q.op", SOLR_QUERY_DEFAULTS.qop); // default op to AND  # applies to dismax and standard query parser

    if (!this.query.includes("*")) {
      // default query parser
      this.params.append("defType", SOLR_QUERY_DEFAULTS.defType);
      this.params.append("qf", SOLR_QUERY_DEFAULTS.qf);
    }

    this.params.append("q", this.query);
    this.params.append("sort", this.sort);
    this.params.append("rows", this.rows);
    this.params.append("facet.mincount", this.facetMinCount);
    this.params.append("start", this.start);
    this.params.append("facet", this.facet);

    if (this.fList) {
      this.params.append("fl", this.fList.join(","));
    }
  }

  addFacetFields(facetFields?: string[]): void {
    const mapOver = facetFields ? facetFields : DEFAULT_FACET_FIELDS;
    mapOver.map((facetField) => this.params.append("facet.field", facetField));
  }

  addFacetInterval(intervalFields: string[], facetIntervals: string[]): void {
    this.params.append(
      "f.n_startYear.facet.limit",
      SOLR_QUERY_DEFAULTS.facetStartYearLimit.toString()
    );
    this.params.append("f.n_endYear.facet.limit", SOLR_QUERY_DEFAULTS.facetEndYearLimit.toString());

    intervalFields.map((intervalField: string) =>
      this.params.append("facet.interval", intervalField)
    );
    facetIntervals.map((facetInterval) => this.params.append("facet.interval.set", facetInterval));
  }

  _fmt(name: string, value: string): string {
    // These are formats to convert the individual field queries into the representation sent to the solr instance.
    // There are basically two formats here -- text/type include parens () around the value, and the others don't.
    const formats: Formats = {
      text: `+${name}:(${value})`,
      type: `+${name}:(${value})`,
      nStartYear: `+n_startYear:${value}`,
      nEndYear: `+n_endYear:${value}`,
    };

    return Object.keys(formats).includes(name)
      ? formats[name as keyof Formats]
      : `+${name}:"${value}"`;
  }

  addFq(name: string, value: string): void {
    this.params.append("fq", this._fmt(name, value));
  }
}

export default SolrQuery;
