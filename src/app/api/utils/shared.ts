import {
  AVAILABLE_FACETS,
  FACET_FIELDS,
  FACET_INTERVALS,
  CATEGORIES,
  COMBINED_TYPES,
  SOLR_RESULT_ITEM_KEYS,
  SOLR_RESULT_CONFIG_KEYS,
  INITIAL_CATECORY_COUNT_MAP,
} from "@/app/api/config/config";
import {
  type Category,
  type SOLRCategories,
  type SOLRCountsArray,
  type FacetFieldKeys,
  type FacetIntervalFieldKeys,
  type ResultItem,
  type ResultItemKeys,
  type ResultConfigKeys,
  type SolrResponseDocsType,
  type FilterEntity,
  type ResultDocs,
  type ResultConfig,
  type ResultKeys,
} from "@/types/types";

export function createSearchParamsMap(nextParams: URLSearchParams) {
  const searchParams = new Map();

  for (const [key, value] of nextParams.entries()) {
    if (["facetName", "facetType"].includes(key)) {
      searchParams.set(key, nextParams.getAll(key));
    } else {
      searchParams.set(key, value);
    }
  }

  return searchParams;
}

export function isFacetIntervalFieldKey(item?: FacetFieldKeys): item is FacetIntervalFieldKeys {
  return item === "documents" || item === "datasets";
}

export function createFacetParams(documentType?: FacetFieldKeys) {
  const facetIntervalFields: string[] = isFacetIntervalFieldKey(documentType)
    ? FACET_INTERVALS[documentType]
    : [];
  const facetFields: string[] = documentType ? FACET_FIELDS[documentType] : AVAILABLE_FACETS;
  const queryFacetFields = Array.from(new Set(facetFields).add("type").values());

  return { facetIntervalFields, queryFacetFields };
}

export function getSolrCategoryCountsMap(
  countsArray: SOLRCountsArray
): Record<SOLRCategories, number> {
  return Object.fromEntries(
    countsArray
      .map((item, idx: number) => {
        if (idx % 2 === 0) {
          return [item, countsArray[idx + 1]];
        }
      })
      .filter((item): item is SOLRCountsArray => !!item) // NOTE: https://learntypescript.dev/07/l6-type-predicate
  );
}

export function updateCategoryCounts(countsMap: Record<SOLRCategories, number>): Category[] {
  const modifiedCategories = structuredClone(CATEGORIES);

  return modifiedCategories.map((category: Category) => {
    // the ts signature for Object.entries is [string, any], but we know we are expecting a [facetFieldKeys, SOLRCategories[]]
    // we thus use the as keyword for type assertion - https://learntypescript.dev/07/l1-type-assertions
    for (const [key, values] of Object.entries(COMBINED_TYPES) as [
      FacetFieldKeys,
      SOLRCategories[],
    ][]) {
      if (category.id === key) {
        category.count = Object.values(
          // gets me list of all counts in the object obtained from Object.entries
          Object.fromEntries(
            // from the combinedType obj, for a given key, get subset of all keys  that are also in the solr counts response map
            values
              .filter((solrCategory: SOLRCategories) =>
                (Object.keys(countsMap) as SOLRCategories[]).includes(solrCategory)
              )
              // chained to subset of COMBINED_TYPES[key], map returns an [[solrCat, count]] which is fed into Object.entries to create a single dict
              .map((solrCategory: SOLRCategories) => [solrCategory, countsMap[solrCategory]])
          )
        ).reduce((acc, curr) => acc + curr, 0);
      }
    }
    return category;
  });
}

export function updateCategoryCountsForSearch(
  countsMap: Record<SOLRCategories, number>
): Record<FacetFieldKeys, number> {
  const updatedCountsMap = structuredClone(INITIAL_CATECORY_COUNT_MAP);
  // return type of keys() function is string[], but we are dealing with FacetFieldKeys hence type assertion
  const facetFieldKeyArr = Object.keys(updatedCountsMap) as FacetFieldKeys[];

  facetFieldKeyArr.map((facetFieldKey: FacetFieldKeys) => {
    for (const [key, values] of Object.entries(COMBINED_TYPES) as [
      FacetFieldKeys,
      SOLRCategories[],
    ][]) {
      if (facetFieldKey === key) {
        updatedCountsMap[facetFieldKey] = Object.values(
          Object.fromEntries(
            values
              .filter((solrCategory: SOLRCategories) =>
                (Object.keys(countsMap) as SOLRCategories[]).includes(solrCategory)
              )
              .map((solrCategory: SOLRCategories) => [solrCategory, countsMap[solrCategory]])
          )
        ).reduce((acc, curr) => acc + curr, 0);
      }
    }
  });

  return updatedCountsMap;
}

export const createResultItem = (item: SolrResponseDocsType): ResultItem => {
  const recordConfigObj = Object.fromEntries(
    SOLR_RESULT_CONFIG_KEYS.map((key: ResultConfigKeys) => {
      return [key, item[key]] as [ResultConfigKeys, string];
    })
  ) as Record<ResultConfigKeys, string>;
  const config: ResultConfig = { recordConfig: recordConfigObj };

  // we do this to get rid of key-values that are unnecessary for the frontend
  const filteredKeys: ResultItemKeys[] = item.keys.filter((key: ResultItemKeys) =>
    SOLR_RESULT_ITEM_KEYS.includes(key)
  );
  const keys: ResultKeys = { keys: filteredKeys };

  const records = Object.fromEntries(
    filteredKeys.map((key: ResultItemKeys) => {
      return [key, item[key]];
    })
  ) as ResultDocs;

  // creating the result item
  return {
    ...records,
    ...keys,
    ...config,
  };
};

export const createResultFilters = (
  facetFields: Record<string, (string | number)[]>
): FilterEntity[] =>
  Object.keys(facetFields)
    .filter((facetKey: string) => facetKey !== "type")
    .map((facetKey: string) => {
      const numbers = facetFields[facetKey].filter(
        (_item): _item is number => typeof _item === "number"
      );
      const strings = facetFields[facetKey].filter(
        (_item: string | number): _item is string => typeof _item === "string"
      );

      return strings.map((item, idx) => ({
        filterName: facetKey,
        itemName: item,
        itemCount: numbers[idx],
      }));
    })
    .reduce((acc, curr) => acc?.concat(curr), []);
