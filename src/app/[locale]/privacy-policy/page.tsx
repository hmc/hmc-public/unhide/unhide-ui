import Link from "next/link";
import { useTranslations } from "next-intl";
import React, { Suspense } from "react";

const PrivacyPolicyPage = () => {
  const t = useTranslations("PrivacyPolicy");

  return (
    <div className="container mx-auto p-20 text-primary-helmholtz-dunkelblau cursor-default">
      <div className="my-10">
        <h1 className="pb-5 font-bold text-2xl">{t("main_heading")}</h1>
        <p className="text-justify">{t("intro")}</p>
      </div>

      <div className="my-10">
        <h3 className="pb-5 text-xl font-bold">{t("definitions")}</h3>
        <p className="text-justify">{t("definitions_intro")}</p>
        {[...Array(11)].map((o, idx) => (
          <div key={idx} className="my-5">
            <h4 className="mb-5 text-lg font-bold">{t(`definitions_${idx + 1}_heading`)}</h4>
            <p className="text-justify">{t(`definitions_${idx + 1}_description`)}</p>
          </div>
        ))}
      </div>

      <div className="my-10">
        <h3 className="pb-5 text-xl font-bold">{t("controller_heading")}</h3>
        <p className="text-justify pb-3 ">{t("controller_intro")}</p>
        <div>{t("fzj_name")}</div>
        <div>{t("fzj_street")}</div>
        <div>{t("fzj_city")}</div>
        <div>{t("fzj_country")}</div>
        <div className="flex items-center">
          <div className="font-semibold mr-1">{t("website")}</div>
          <Link
            href={t("controller_dpo_website")}
            className="italic underline hover:text-primary-helmholtz-hellblau"
          >
            {t("controller_dpo_website")}
          </Link>
        </div>
        <div className="flex items-center">
          <div className="font-semibold mr-1">{t("email")} </div>
          <Link
            href={`mailto:${t("controller_email")}`}
            className="italic underline hover:text-primary-helmholtz-hellblau"
          >
            {t("controller_email")}
          </Link>
        </div>
      </div>

      <div className="my-10">
        <h3 className="pb-5 text-xl font-bold">{t("dpo_heading")}</h3>
        <p className="pb-2">{t("dpo_intro")}</p>
        <div className="font-semibold italic">{t("dpo_name")}</div>
        <div>{t("fzj_name")}</div>
        <div>{t("fzj_street")}</div>
        <div>{t("fzj_city")}</div>
        <div>{t("fzj_country")}</div>
        <div className="flex items-center">
          <div className="font-semibold mr-1">{t("website")}</div>
          <Link
            href={t("controller_dpo_website")}
            className="italic underline hover:text-primary-helmholtz-hellblau"
          >
            {t("controller_dpo_website")}
          </Link>
        </div>

        <div className="flex items-center">
          <div className="font-semibold mr-1">{t("email")}</div>
          <Link
            href={`mailto:${t("dpo_email")}`}
            className="italic underline hover:text-primary-helmholtz-hellblau"
          >
            {t("dpo_email")}
          </Link>
        </div>

        <div>
          <span className="font-semibold mr-1">{t("telephone")}</span>
          <span>{t("dpo_telephone_number")}</span>
        </div>
        <p className="pt-2">{t("dpo_outro")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("collection_info_heading")}</h2>
        <p className="text-justify">{t("collection_info")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("newsletter_subscription_heading")}</h2>
        <p className="text-justify">{t("newsletter_subscription")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("contact_heading")}</h2>
        <p className="text-justify">{t("contact")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("erasure_heading")}</h2>
        <p className="text-justify">{t("erasure")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("data_subject_heading")}</h2>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_1_heading")}</h3>
        <p>{t("data_subject_1")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_2_heading")}</h3>
        <p>{t("data_subject_2_para_1")}</p>
        {[...Array(8)].map((o, idx) => (
          <ol key={idx} className=" my-5">
            <li>- {t(`data_subject_2_${idx + 1}`)}</li>
          </ol>
        ))}
        <p>{t("data_subject_2_para_2")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_3_heading")}</h3>
        <p>{t("data_subject_3")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_4_heading")}</h3>
        <p>{t("data_subject_4_para_1")}</p>
        {[...Array(6)].map((o, idx) => (
          <ul key={idx} className=" my-5">
            <li>- {t(`data_subject_4_${idx + 1}`)}</li>
          </ul>
        ))}
        <p>{t("data_subject_4_para_2")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_5_heading")}</h3>
        <p>{t("data_subject_5_para_1")}</p>
        {[...Array(4)].map((o, idx) => (
          <ul key={idx} className="my-5">
            <li>- {t(`data_subject_5_${idx + 1}`)}</li>
          </ul>
        ))}
        <p>{t("data_subject_5_para_2")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_6_heading")}</h3>
        <p>{t("data_subject_6")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_7_heading")}</h3>
        <p>{t("data_subject_7")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_8_heading")}</h3>
        <p>{t("data_subject_8")}</p>
        <h3 className="py-5 text-lg font-bold">{t("data_subject_9_heading")}</h3>
        <p className="pb-2">{t("data_subject_9_para_1")}</p>
        <div className="flex items-center">
          <div className="font-semibold mr-1">{t("email")}</div>
          <Link
            href={`mailto:${t("controller_email")}`}
            className="italic underline hover:text-primary-helmholtz-hellblau"
          >
            {t("controller_email")}
          </Link>
        </div>

        <p className="pt-2">{t("data_subject_9_para_2")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("processing_heading")}</h2>
        <p className="text-justify">{t("processing")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("legitimate_interest_heading")}</h2>
        <p className="text-justify">{t("legitimate_interest")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("data_stored_heading")}</h2>
        <p className="text-justify">{t("data_stored")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("provision_personal_data_heading")}</h2>
        <p className="text-justify">{t("provision_personal_data")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("automated_decision_heading")}</h2>
        <p className="text-justify">{t("automated_decision")}</p>
      </div>

      <div className="my-10">
        <h2 className="pb-5 text-xl font-bold">{t("amendment_heading")}</h2>
        <p className="text-justify">{t("amendment")}</p>
      </div>
    </div>
  );
};

export default function PrivacyPolicy() {
  return (
    <Suspense>
      <PrivacyPolicyPage />
    </Suspense>
  );
}
