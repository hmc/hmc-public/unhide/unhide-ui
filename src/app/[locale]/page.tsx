"use client";

import Categories from "@/components/app/Categories";
import Intro from "@/components/app/Intro";
import MobileSearch from "@/components/layout/MobileSearch";

const Home = () => {
  return (
    <div className="grow flex flex-col justify-start ">
      {/* search component shown on mobile and ipad*/}
      <MobileSearch />

      {/* intro component from laptop, desktops and more */}
      <div>
        <Intro />
      </div>

      {/* categories list for all views  */}
      <div className="grow flex">
        <Categories />
      </div>
    </div>
  );
};

export default Home;
