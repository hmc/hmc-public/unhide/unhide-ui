import type { Metadata, Viewport } from "next";
import "./globals.css";
import { notFound } from "next/navigation";
import { useTranslations, useMessages, NextIntlClientProvider } from "next-intl";
import React from "react";

import Banner from "@/components/layout/Banner";
import Footer from "@/components/layout/Footer";
import Header from "@/components/layout/Header";
import { routing } from "@/i18n/routing";

const env = process.env.NEXT_PUBLIC_SHOW_BANNER;
const showBanner = env && (env.toLowerCase() === "true" || env === "1");

export const metadata: Metadata = {
  title: "Helmholtz KG",
  description: "Description for the HKG needed",
  icons: {
    icon: [
      { url: "/favicon-32x32.png", sizes: "32x32" },
      { url: "/favicon-16x16.png", sizes: "16x16" },
    ],
    apple: { url: "/apple-touch-icon.png", sizes: "180x180" },
  },
  manifest: "/site.webmanifest",
  other: {
    UnHIDE: "UnHIDE Search",
    HelmholtzKG: "Helmholtz Knowledge Graph",
  },
};

export const viewport: Viewport = {
  width: "device-width",
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
};

export default function RootLayout({
  children,
  params,
}: Readonly<{
  children: React.ReactNode;
  params: Promise<{ locale: string }>;
}>) {
  const { locale } = React.use(params);
  // Ensure that the incoming `locale` is valid
  if (!routing.locales.includes(locale as any)) {
    notFound();
  }

  const t = useTranslations("Layout");
  const messages = useMessages();

  return (
    <html lang={locale}>
      <body>
        <noscript>{t("js_needed")}</noscript>
        <main className="flex flex-col">
          <NextIntlClientProvider locale={locale} messages={messages}>
            <header className="sticky top-0 w-full border-b-2 border-b-secondary-helmholtz-mint shadow-lg bg-primary-helmholtz-weiss py-5 ">
              {showBanner && <Banner />}
              <Header />
            </header>
            <div className="grow flex flex-col">{children}</div>
            <div className="sticky bottom-0">
              <Footer />
            </div>
          </NextIntlClientProvider>
        </main>
      </body>
    </html>
  );
}
