"use client";

import { useSearchParams } from "next/navigation";
import React, { Suspense, useEffect, useRef, useState } from "react";

import BackBar from "@/components/app/results/BackBar";
import CategoryBar from "@/components/app/results/CategoryBar";
import ListResults from "@/components/app/results/ListResults";
import Error from "@/components/layout/Error";
import MobileSearch from "@/components/layout/MobileSearch";
import Spinner from "@/components/layout/Spinner";
import type { Category } from "@/types/types";
import { getAllCategories, getCategoryCounts } from "@/utils/apiCall";

const ResultsComponent = () => {
  const resultParams = useSearchParams();

  const [categories, setCategories] = useState<Category[]>([]);
  const [baseCategories, setBaseCategories] = useState<Category[]>([]);
  const [activeCategory, setCategory] = useState<Category>();
  const [categoryLoading, setCategoryLoading] = useState(true);
  const [hasBcError, setHasBcError] = useState(false);
  const [hasSearchError, setHasSearchError] = useState(false);

  const firstRender = useRef(true);

  const category = resultParams.get("category");
  const searchText = resultParams.get("searchText") ?? undefined;

  useEffect(() => {
    const loadCategories = async () => {
      setCategoryLoading(true);
      try {
        const categories = await getAllCategories();
        setBaseCategories(categories);
      } catch {
        setHasBcError(true);
      } finally {
        setCategoryLoading(false);
      }
    };

    loadCategories().catch(console.error);
  }, []);

  useEffect(() => {
    if (!activeCategory && categories.length) {
      setCategory(categories[0]);
    }
  }, [activeCategory, categories]);

  useEffect(() => {
    categories.forEach((cat: Category) => (cat.id === category ? setCategory(cat) : null));
  }, [category, categories]);

  useEffect(() => {
    const updateCategoryCount = async () => {
      try {
        setHasSearchError(false);
        const categoryCounts = await getCategoryCounts(searchText);
        const categories = structuredClone(baseCategories);

        categories.forEach((cat: Category) => (cat.count = categoryCounts[cat.id] ?? 0));
        setCategories(categories);
      } catch (error) {
        console.error("Failed to fetch category counts", error);
        setHasSearchError(true);
      } finally {
        setCategoryLoading(false);
      }
    };

    if (firstRender.current) {
      firstRender.current = false;
    } else {
      updateCategoryCount().catch(console.error);
    }
  }, [baseCategories, searchText]);

  return (
    <div className="grow flex flex-col justify-start">
      {categoryLoading ? (
        <Spinner />
      ) : hasBcError || hasSearchError ? (
        <Error />
      ) : (
        <>
          <CategoryBar
            categories={categories}
            category={activeCategory}
            setCategory={setCategory}
          />
          <div className="flex flex-col md:px-10 lg:px-16 py-5 mb-8 bg-whitesmoke h-full">
            <BackBar />
            <MobileSearch />
            {activeCategory && <ListResults activeCategory={activeCategory} />}
          </div>
        </>
      )}
    </div>
  );
};

export default function Results() {
  return (
    <Suspense>
      <ResultsComponent />
    </Suspense>
  );
}
