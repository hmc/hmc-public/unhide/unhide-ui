import { Category, FilterEntity, SearchResponse, RorResultItem } from "@/types/types";
const baseUrl = process.env.NEXT_PUBLIC_API_URL || "";

async function searchAPI<T>(
  searchText?: string,
  documentType?: string,
  rows?: number,
  start?: number,
  filters?: FilterEntity[]
): Promise<T> {
  const params = new URLSearchParams();

  start !== undefined && params.append("start", start.toString());
  rows !== undefined && params.append("rows", rows.toString());
  documentType !== undefined && params.append("documentType", documentType);
  searchText !== undefined && params.append("searchText", searchText);

  if (filters === undefined) {
    params.append("includeFacets", "false");
  } else {
    filters.forEach((entity) => {
      params.append("facetType", entity.filterName);
      params.append("facetName", entity.itemName);
    });
  }

  const res = await fetch(`${baseUrl}/api/search?${params}`, {
    headers: {
      Accept: "application/json",
    },
  });

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export async function getAllCategories(): Promise<Category[]> {
  const res = await fetch(`${baseUrl}/api/categories`, {
    headers: {
      Accept: "application/json",
    },
  });
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }
  return res.json();
}

export async function getCategoryCounts(searchText?: string): Promise<Record<string, number>> {
  const result: SearchResponse = await searchAPI(searchText);
  return result.categoryCounts;
}

export async function getRorInfo(rorId: string): Promise<RorResultItem> {
  const rorItem: RorResultItem = { id: "", name: "", status: "", established: 0, links: [] };
  const response = await fetch(`https://api.ror.org/organizations/${rorId}`, {
    headers: {
      Accept: "application/json",
    },
  });
  const rorResult: RorResultItem = { ...rorItem, ...(await response.json()) };
  return rorResult;
}

export async function search(
  documentType?: string,
  searchText?: string,
  rows: number = 10,
  start: number = 0,
  filters: FilterEntity[] = []
): Promise<SearchResponse> {
  try {
    return await searchAPI(searchText, documentType, rows, start, filters);
  } catch (error) {
    // TODO: Error handling
    throw error;
  }
}
