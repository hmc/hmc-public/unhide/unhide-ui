import type { Config } from "tailwindcss";

const config: Config = {
  content: ["./src/{components, app}/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {
      screens: {
        "3xl": "2500px",
      },
      colors: {
        "primary-helmholtz-dunkelblau": "#002864",
        "primary-helmholtz-hellblau": "#14c8ff",
        "primary-helmholtz-weiss": "#ffffff",
        "secondary-helmholtz-logoblau": "#005aa0",
        "secondary-helmholtz-highlightblau": "#cdeefb",
        "secondary-helmholtz-webblassblau": "#ecfbfd",
        "secondary-helmholtz-mint": "#05e5ba",
        "secondary-helmholtz-claimgrun": "#008040",
        "tertiary-helmholtz-leuchtgrun": "#8cd600",
        "tertiary-helmholtz-leuchtorange": "#fa7833",
        "tertiary-helmholtz-leuchtpink": "#ed5ef2",

        "primary-juelich-darkblue": "#023d6b",
        "primary-juelich-lightblue": "#adbde3",
        "secondary-juelich-lightgrey": "#ebebeb",
        "secondary-juelich-lemon": "#faeb5a",
        "secondary-juelich-raspberry": "#eb5f73",
        "secondary-juelich-grassgreen": "#b9d25f",
        "secondary-juelich-violet": "#af82b9",
        "secondary-juelich-apricot": "#fab45a",

        whitesmoke: "#f6f6f6",
      },
      fontFamily: {},
    },
  },
  plugins: [require("@headlessui/tailwindcss")({ prefix: "ui" })],
};
export default config;
