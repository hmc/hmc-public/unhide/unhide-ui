# Welcome

To the **Un**ified **H**elmholtz **I**nformation and **D**ata **E**xhchange (**UnHIDE**) project - an initiative of the **H**elmholtz **K**nowledge **G**raph (**HKG**). The project allows easy access via simple and user-friendly interfaces to the existing and emerging data systems that are linked via the HKG. We aim to build a sustainable, interoperable, and an inclusive digital ecosystem for all stakeholders of the HKG from within and outside of the Helmholtz Association (**HGF**). Specifically we aim bring value by working towards

- increasing discoverability and actionability of HGF data across the whole association
- movtivating the enhancement of the quality of (meta-)data generated
- providing overviews and diagnostics of teh HGF data and digital-asset space
- allowing traceable and reproducible recovery of (meta-)data to enhance research
- supporting connectivity of HGF data to interact with global infrastructures and projects
- acting as a central access and distribution point for stakeholders within and beyond the HGF

The project contains the implementation of two user-facing interfaces for the HKG - a web application and an accessible set of web APIs, both of which can be used to search the HKG. It is via these interfaces we hope to coordinate action and capacity to improve access to scientific publications, software, data and knowledge.

## Getting Started

If you have just cloned this repository, please navigate to the folder that contains the `package.json` file for the repository and start by installing all dependencies:

```bash
npm install
# or
yarn install
```

To run the development server:

As a standalone and locally-run application, you will need to create a `.env.development.local` file to store the environment variables required to run this application. The variable names can be found in `.env.example`. The values can be obtained from one of the developers that work on this repository (see section below). Thereafter,

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the web application running.
Open [http://localhost:3000/api/hello](http://localhost:3000/api/hello) with your browser to access the example web-API, to get started.

The run the application in production:

The application is one service that is part of the unhide project. It is set up as a service via a `docker compose` file in the unhide-docker repository. And as such, the details of how the application is deployed to production can be found in the developer wiki of that repository.

## Learn More

The details of solution architecture, design decision and other notes on development details can be found in the repository [wiki](https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/unhide-ui/-/wikis/home) for developers who are interested in joining and actively contributing to the projects development. In case of further questions concerning development, please reach out to [Ms.Fiona D'Mello](mailto:f.dmello@fz-juelich.de) or [Mr.Gabriel Preuß](mailto:gabriel.preuss@helmholtz-berlin.de), both of whom contribute and maintain this project.

## Acknowledgement

<div>
<img style="vertical-align: middle;" alt="HMC Logo" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/raw/main/HMC/HMC_Logo_M.png" width=50% height=50% />
&nbsp;&nbsp;
<img style="vertical-align: middle;" alt="FZJ Logo" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/raw/main/FZJ/FZJ.png" width=30% height=30% />
</div>
<br />

The project is implemented by the **H**elmholtz **M**etadata **C**ollaboration (**HMC**) an incubator platform of the Helmholtz Association. It is funded by the Helmholtz Association. The project is deployed by **J**ülich **S**uper **C**omputing Cloud (**JSC Cloud**) at **F**orschungs**z**entrum **J**ülich GmbH (**FZJ**), an organization that is part of the Helmholtz Association. We thank all these stakeholders for their support and contribution to this projects development.
