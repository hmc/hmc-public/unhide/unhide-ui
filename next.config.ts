import createNextIntlPlugin from "next-intl/plugin";
import type { NextConfig } from "next";

const withNextIntl = createNextIntlPlugin();

const nextConfig: NextConfig = {
  // reactStrictMode: false,
  eslint: {
    dirs: ["src"],
  },
  async headers() {
    return [
      {
        source: "/api/:path*",
        headers: [
          {
            key: "Access-Control-Allow-Origin",
            value: "*", // Set your origin
          },
          {
            key: "Access-Control-Allow-Methods",
            value: "GET, OPTIONS",
          },
        ],
      },
      {
        source: "/:path*",
        headers: [
          {
            key: "X-Frame-Options",
            value: "DENY",
          },
          {
            key: "Content-Security-Policy",
            value: "frame-src 'none'",
          },
        ],
      },
    ];
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "www.helmholtz.de",
        port: "",
        pathname:
          "/typo3conf/ext/dreipc_hgf/Resources/Public/Frontend/Build/assets/icons/favicons/icon-228x228.png",
      },
      {
        protocol: "https",
        hostname: "www.awi.de",
        port: "",
        pathname: "/_assets/978631966794c5093250775de182779d/Images/AWI/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "www.geomar.de",
        port: "",
        pathname:
          "/typo3conf/ext/geomar_provider/Resources/Public/Graphics/favicon/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "hereon.de",
        port: "",
        pathname: "/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "www.mdc-berlin.de",
        port: "",
        pathname: "/sites/default/files/favicons/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "www.desy.de",
        port: "",
        pathname: "/++resource++desy/images/desy_logo_3c_web.svg",
      },
      {
        protocol: "https",
        hostname: "www.helmholtz-hzi.de",
        port: "",
        pathname: "/_assets/ae8a1fe32f30b295243325c1db1b8058/Icons/Favicons/HZI/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "www.gfz-potsdam.de",
        port: "",
        pathname: "/typo3conf/ext/gfz_www_sitepackage/Resources/Public/Icons/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "www.helmholtz-berlin.de",
        port: "",
        pathname: "/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "www.ufz.de",
        port: "",
        pathname: "/static/custom/weblayout/DefaultInternetLayout/img/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "www.hzdr.de",
        port: "",
        pathname: "/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "www.dlr.de",
        port: "",
        pathname: "/icon.svg",
      },
      {
        protocol: "https",
        hostname: "www.gsi.de",
        port: "",
        pathname: "/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "www.helmholtz-munich.de",
        port: "",
        pathname:
          "/typo3conf/ext/helmholtz_sitepackage/Resources/Public/Icons/Favicons/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "www.kit.edu",
        port: "",
        pathname: "/img/intern/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "www.dkfz.de",
        port: "",
        pathname: "/global/img/favicon.ico",
      },
      {
        protocol: "https",
        hostname: "cispa.de",
        port: "",
        pathname: "/images/cispa-logo-on-bright.svg",
      },
      {
        protocol: "https",
        hostname: "www.dzne.de",
        port: "",
        pathname: "/typo3conf/ext/fe_website/Resources/Public/AppIcons/favicon-16x16.png",
      },
      {
        protocol: "https",
        hostname: "www.fz-juelich.de",
        port: "",
        pathname: "/icon.svg",
      },
    ],
  },
};

export default withNextIntl(nextConfig);
